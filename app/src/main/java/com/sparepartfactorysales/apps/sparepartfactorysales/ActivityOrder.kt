package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.RecyclerViewAdapterOrderList
import com.sparepartfactorysales.apps.sparepartfactorysales.model.RecyclerViewModelOrder
import kotlinx.android.synthetic.main.activity_order.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import kotlin.properties.Delegates

class ActivityOrder: AppCompatActivity(), View.OnClickListener {

    companion object {
        val REQUEST_CODE = 15
        var mflag: String? = null
        fun launchIntent(caller: Activity?, flag: String?){
            val intent = Intent(caller, ActivityOrder::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)

            mflag = flag
        }
    }

    override fun onClick(p0: View?) {
        when(p0){
            order_button_add ->{
                ActivityCatalog.launchIntent(this@ActivityOrder, "else")
            }

            order_button_submit ->{
                ActivityDetailOrder.launchIntent(this, mflag)
            }

            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }

    }

    val dataOrder = ArrayList<RecyclerViewModelOrder>()
    var adapter by Delegates.notNull<RecyclerViewAdapterOrderList>()
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        toolbar_order.title_toolbar.text = "ORDER"

        dataOrder.add(RecyclerViewModelOrder(R.drawable.banner_promo, "Birkens", "Wheel Cylinder", "Daihatsu", "47550-87511", "DELTA(V22)", "10 pcs", "Rp 1.170.000"))
        dataOrder.add(RecyclerViewModelOrder(R.drawable.banner_promo, "Birkens", "Wheel Cylinder", "Daihatsu", "47550-87511", "DELTA(V22)", "10 pcs", "Rp 1.170.000"))

        adapter = RecyclerViewAdapterOrderList(dataOrder, this@ActivityOrder, object: RecyclerViewAdapterOrderList.RecyclerViewAdapterListener{
            override fun getData() {

            }

        })
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this@ActivityOrder)
        order_recyclerview.layoutManager = layoutManager
        order_recyclerview.adapter = adapter

        setClickLIstener()
    }

    fun setClickLIstener(){
        order_button_add.setOnClickListener(this)
        order_button_submit.setOnClickListener(this)
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }
}