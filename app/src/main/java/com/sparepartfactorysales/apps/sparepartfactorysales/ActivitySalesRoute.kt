package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.sparepartfactorysales.apps.sparepartfactorysales.fragment.FragmentAddStoreSalesRoute
import com.sparepartfactorysales.apps.sparepartfactorysales.model.Model
import kotlinx.android.synthetic.main.activity_sales_route.*
import kotlinx.android.synthetic.main.dialog_fragment_add_sales_route.view.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*





class ActivitySalesRoute: AppCompatActivity(), View.OnClickListener, FragmentAddStoreSalesRoute.ApprovalActionListenerReject {
    override fun onApprovalResultReject(rejectDesc: String?) {
        Toast.makeText(this@ActivitySalesRoute, "Hasilzzz: "+rejectDesc, Toast.LENGTH_SHORT).show()
        print(rejectDesc)
        createMenuDropdown(rejectDesc)
    }

    override fun onClick(p0: View?) {
        when(p0){
            sales_route_button_search ->{
                val bottomSheetDialogFragment = FragmentAddStoreSalesRoute()
                /*b.putStringArrayList("mulitData", kode)
                b.putStringArrayList("mulitDataDesk", desk)
                bottomSheetDialogFragment.arguments = b*/
                bottomSheetDialogFragment.mListenerReject = this
                //......
                bottomSheetDialogFragment.show(supportFragmentManager, bottomSheetDialogFragment.tag)
            }
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sales_route)

        toolbar_sales_route.title_toolbar.text = "SALES ROUTE"

        setClickListener()

    }

    fun setClickListener(){
        sales_route_button_search.setOnClickListener(this)
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }

    fun createMenuDropdown(data: String?){
        val parent = RelativeLayout(this)
                val param = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                parent.layoutParams = param
                parent.setPadding(30, 15, 30, 15)
                parent.setBackgroundColor(getResources().getColor(R.color.colorBlackBackgroundButton))
                param.topMargin = 100
                parent.setTag(data)


                //titleLayout
                val titleViewParam = RelativeLayout.LayoutParams(
                                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                                        RelativeLayout.LayoutParams.WRAP_CONTENT)

                val titleLayoutStore = TextView(this)
                titleLayoutStore.layoutParams = titleViewParam
                titleLayoutStore.setTextColor(getResources().getColor(R.color.colorYellowTextButton))
                titleLayoutStore.setText(""+data)
                titleLayoutStore.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20F)
                titleViewParam.addRule(RelativeLayout.ALIGN_PARENT_LEFT)

                //imageViewLayout
                val imageViewParam = RelativeLayout.LayoutParams(
                                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                                        RelativeLayout.LayoutParams.WRAP_CONTENT)
                val imageDropDown = ImageView(this)
                imageDropDown.layoutParams = imageViewParam
                imageDropDown.background = resources.getDrawable(R.drawable.baseline_expand_more_24)
                imageViewParam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)


                val viewParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,100)
                val v1 = View(this)
                v1.layoutParams = viewParam
                v1.setBackgroundColor(getResources().getColor(R.color.colorBackgroundStatistic))
                v1.tag = data

                //button
                val layoutButtonDropdown = LinearLayout(this)
                layoutButtonDropdown.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                layoutButtonDropdown.setOrientation(LinearLayout.HORIZONTAL)
                layoutButtonDropdown.setWeightSum(1f)
                layoutButtonDropdown.setVisibility(View.GONE)
                layoutButtonDropdown.tag = data


                //button visit
                val subparent1 = LinearLayout(this)
                subparent1.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.33f)
                subparent1.setOrientation(LinearLayout.VERTICAL)

                val buttonVisitViewParam = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT)
                val subparenttv1 = Button(this)
                subparenttv1.layoutParams = buttonVisitViewParam
                subparenttv1.setTextColor(getResources().getColor(R.color.colorBlack))
                subparenttv1.setText("Visit")
                subparenttv1.background = resources.getDrawable(R.drawable.black_box)
                subparenttv1.setOnClickListener(object: View.OnClickListener{
                    override fun onClick(v: View?) {
                        //ActivityCheckIn.launchIntent(this@ActivitySalesRoute)
                        startActivity(Intent(this@ActivitySalesRoute, ActivityCheckIn::class.java))
                    }

                })
                subparent1.addView(subparenttv1)

                //Button Survey
                val subparent2 = LinearLayout(this)
                subparent2.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.33f)
                subparent2.setOrientation(LinearLayout.VERTICAL)

                val buttonSurveyViewParam = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT)
                val subparent2tv2 = Button(this)
                subparent2tv2.layoutParams = buttonSurveyViewParam
                subparent2tv2.setTextColor(getResources().getColor(R.color.colorBlack))
                subparent2tv2.setText("Survey")
                subparent2tv2.background = resources.getDrawable(R.drawable.black_box)
                subparent2tv2.setOnClickListener(object: View.OnClickListener{
                    override fun onClick(v: View?) {
                        ActivitySurvey.launchIntent(this@ActivitySalesRoute)
                        //startActivity(Intent(this@ActivitySalesRoute, ActivitySurvey::class.java))
                    }

                })

                subparent2.addView(subparent2tv2)


                //Button Collection
                val subparent3 = LinearLayout(this)
                subparent3.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.34f)
                subparent3.setOrientation(LinearLayout.VERTICAL)

                val buttonCollectionViewParam = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT)
                val subparent3tv3 = Button(this)
                subparent3tv3.layoutParams = buttonCollectionViewParam
                subparent3tv3.setTextColor(getResources().getColor(R.color.colorBlack))
                subparent3tv3.setText("Collection")
                subparent3tv3.background = resources.getDrawable(R.drawable.black_box)
                subparent3tv3.setOnClickListener(object: View.OnClickListener{
                    override fun onClick(v: View?) {
                        //ActivityCollection.launchIntent(this@ActivitySalesRoute)
                        startActivity(Intent(this@ActivitySalesRoute, ActivityCollection::class.java))
                    }

                })

                subparent3.addView(subparent3tv3)

                layoutButtonDropdown.addView(subparent1)
                layoutButtonDropdown.addView(subparent2)
                layoutButtonDropdown.addView(subparent3)

                parent.addView(titleLayoutStore)
                parent.addView(imageDropDown)
                parent.setOnClickListener {
                    if (layoutButtonDropdown.visibility == View.GONE) {
                        layoutButtonDropdown.visibility = View.VISIBLE
                    } else {
                        layoutButtonDropdown.visibility = View.GONE
                    }
                }


                sales_route_layout_bottom.addView(parent)
                sales_route_layout_bottom.addView(layoutButtonDropdown)
                sales_route_layout_bottom.addView(v1)
    }
}