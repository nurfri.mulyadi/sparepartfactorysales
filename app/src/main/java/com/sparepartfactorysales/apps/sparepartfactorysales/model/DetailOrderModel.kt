package com.sparepartfactorysales.apps.sparepartfactorysales.model

class DetailOrderModel(code: String?, qty: String?, uom: String?, price: String?, total: String?, detail: String?){
    val mCode = code
    val mQty = qty
    val mUom = uom
    val mPrice = price
    val mTotal = total
    val mDetail = detail
}