package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_decription_product.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*

class ActivityDescriptionProduct: AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {
        when(p0){
            description_product_button_read_more ->{
                description_product_content.visibility = View.VISIBLE
                description_product_button_read_more.visibility = View.GONE
            }
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    companion object {
        val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityDescriptionProduct::class.java)
                caller!!.startActivityForResult(intent, REQUEST_CODE)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_decription_product)

        toolbar_description_product.title_toolbar.text = "PRODUCT KNOWLEDGE"

        setContent()
        setClickListener()
    }

    fun setClickListener(){
        description_product_button_read_more.setOnClickListener(this)
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }

    fun setContent(){
        description_product_title.text = "BRAKE MASTER(MASTER REM),CM,CO dan WHEEL CYLINDER"
        description_product_datePost.text = "2014-06-10 23:29:22"
        description_product_title_content.text = "APA FUNGSI BRAKER MASTER?"
        description_product_content.text = "Apa fungsi braker master apa fungsi braker master apa fungsi braker master apa fungsi braker master apa fungsi braker master apa fungsi braker master apa fungsi braker master apa fungsi braker master."
        description_product_img.background = resources.getDrawable(R.drawable.img_product)
    }

}