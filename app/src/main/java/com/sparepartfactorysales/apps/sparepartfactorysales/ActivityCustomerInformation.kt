package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.RecyclerViewCustomerInformationAdapter
import kotlinx.android.synthetic.main.activity_customer_information.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlin.properties.Delegates
import kotlinx.android.synthetic.main.toolbar.view.*

class ActivityCustomerInformation: AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {
        when(p0){
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    companion object {
        /*val REQUEST_CODE = 15
        fun launchIntent(caller:  Activity?){
            val intent = Intent(caller, ActivityCustomerInformation::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }*/
    }
    fun setonclicklistener(){
        toolbar_customer_information.toolbar_button_back.setOnClickListener(this)
    }

    val dataCustomerInformation = ArrayList<String>()
    var adapter by Delegates.notNull<RecyclerViewCustomerInformationAdapter>()
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_information)

        toolbar_customer_information.title_toolbar?.text = "CUSTOMER INFORMATION"

        setClickListener()

        dataCustomerInformation.add("TOKO SINAR ABADI")
        dataCustomerInformation.add("TOKO BHAKTI GUNA")
        dataCustomerInformation.add("TOKO CEMERLANG JAYA")
        dataCustomerInformation.add("TOKO BINTANG TERANG")


        adapter = RecyclerViewCustomerInformationAdapter(dataCustomerInformation, this@ActivityCustomerInformation, object: RecyclerViewCustomerInformationAdapter.RecyclerViewAdapterListener{
            override fun getData() {
                //ActivityCustomerDetail.launchIntent(this@ActivityCustomerInformation)
                startActivity(Intent(this@ActivityCustomerInformation, ActivityCustomerDetail::class.java))
            }

        })
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this@ActivityCustomerInformation)
        customer_information_recyclerview.layoutManager = layoutManager
        customer_information_recyclerview.adapter = adapter


    }

    fun setClickListener(){
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }

}