package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_customer_detail.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*

class ActivityCustomerDetail: AppCompatActivity(), View.OnClickListener, OnMapReadyCallback, LocationListener {

    override fun onClick(p0: View?) {
        when(p0){
            customer_detail_button_order_history ->{
//                ActivityOrderHistory.launchIntent(this)
                startActivity(Intent(this@ActivityCustomerDetail, ActivityOrderHistory::class.java))
            }
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    companion object {
        /*val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityCustomerDetail::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }*/
    }

    var locationManager: LocationManager? = null
    private var mMap: GoogleMap? = null
    var lat: Double? = null
    var long: Double? = null
    var Nama: String? = null
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_detail)

        toolbar_customer_detail.title_toolbar.text = "CUSTOMER DETAIL"

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.customer_detail_map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        lat = -6.224500
        long = 106.840374
        Nama = "Wisma Staco"

        setClickListener()
    }

    fun setClickListener(){
        customer_detail_button_order_history.setOnClickListener(this)
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        enableMyLocation()

        val lat = this.lat
        val lon = this.long
        val sydney = LatLng(lat!!, lon!!)
        mMap!!.addMarker(MarkerOptions().position(sydney).title("Marker in $Nama"))
        val zoomLevel = 17.0f

        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel))
        mMap!!.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
            override fun onMarkerClick(marker: Marker): Boolean {
                return false
            }
        })
    }

    override fun onLocationChanged(location: Location?) {

    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }

    //===================================Method enableMyLocation===========================================
    //mendapatkan hak akses permission untuk mendapatkan lokasi user (Untuk di atas API 23)
    fun enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap!!.setMyLocationEnabled(true)
        } else {

        }
    }
}