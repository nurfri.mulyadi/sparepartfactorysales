package com.sparepartfactorysales.apps.sparepartfactorysales.session

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v4.content.ContextCompat.startActivity
import com.sparepartfactorysales.apps.sparepartfactorysales.ActivityHome
import com.sparepartfactorysales.apps.sparepartfactorysales.MainActivity

class SessionManager{

    var pref: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null
    var _context: Context? = null
    val PREF_NAME = "user32"
    val PRIVATE_MODE = 0
    val TOKEN = "token"
    val USERID = "userid"
    val ROLEUSER = "roleuser"
    val is_LOGIN = "isLogedIn"

    fun sessions(context: Context): SessionManager{
        this._context = context
        pref = _context!!.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref!!.edit()

        return this
    }

    fun createSessionLogin(token: String, userid: String, roleuser: String){
        editor!!.putBoolean(is_LOGIN, true)
        editor!!.putString(TOKEN, token)
        editor!!.putString(USERID, userid)
        editor!!.putString(ROLEUSER, roleuser)
        editor!!.commit()
    }

    fun getUserDetails(): HashMap<String, String>{
        val user = HashMap<String, String>()
        user[TOKEN] = pref!!.getString(TOKEN, null)
        user[USERID] = pref!!.getString(USERID, null)
        user[ROLEUSER] = pref!!.getString(ROLEUSER, null)

        return user
    }

    fun checkSessionLogin(activity: Activity){
        // Check login status
        if (!this.isLogedIn()) {
            // user is not logged in redirect him to Login Activity
            //MainActivity.launchIntent(activity)
        }else if (this.isLogedIn()){
            /*val i = Intent(_context, ActivityLogout::class.java)
            _context!!.startActivity(i)*/
            //ActivityHome.launchIntent(activity)
            //startActivity(Intent(activity, ActivityHome::class.java))
        }
    }

    fun logoutSession(){
        editor!!.clear()
        editor!!.commit()
    }

    fun isLogedIn(): Boolean{
        return pref!!.getBoolean(is_LOGIN, false)
    }
}