package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.sparepartfactorysales.apps.sparepartfactorysales.model.OrderHistoryModel
import kotlinx.android.synthetic.main.activity_order_history.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.util.*

class ActivityOrderHistory: AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {
        when(p0){
            order_history_button_search ->{

            }
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    companion object {
        val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityOrderHistory::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }
    }

    var mStartDateText: EditText? = null
    var mEndDateText: EditText? = null
    val dataOrderHistory = ArrayList<OrderHistoryModel>()
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_history)

        toolbar_order_history.title_toolbar.text = "ORDER HISTORY"

        mStartDateText = order_history_input_start_date as EditText
        mEndDateText = order_history_input_end_date as EditText

        setClickListener()
        dataPicker()
        createListStatusProduct()
    }

    fun setClickListener(){
        order_history_button_search.setOnClickListener(this)
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }

    private fun dataPicker(){
        mStartDateText?.keyListener = null
        mStartDateText?.setOnClickListener {
            val currDate = mStartDateText!!.text
            val currCalendar = Calendar.getInstance()
            val currYear = if (currDate!!.isNotBlank()) Integer.parseInt(currDate.substring(0, 4)) else currCalendar.get(Calendar.YEAR)
            val currMonth = if (currDate.isNotBlank()) Integer.parseInt(currDate.substring(5, 7)) - 1 else currCalendar.get(Calendar.MONTH)
            val currDay = if (currDate.isNotBlank()) Integer.parseInt(currDate.substring(8)) else currCalendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        val datePicked = year.toString() + "-" + (if (monthOfYear + 1 < 10) "0" +
                                (monthOfYear + 1) else monthOfYear + 1) + "-" +
                                if (dayOfMonth < 10) "0" + dayOfMonth else dayOfMonth
                        mStartDateText?.setText(datePicked)
                    }, currYear, currMonth, currDay)

            val endDate = mEndDateText?.text
            val limCalendar = Calendar.getInstance()
            if (endDate!!.isNotBlank()) {
                val limYear = Integer.parseInt(endDate.substring(0, 4))
                val limMonth = Integer.parseInt(endDate.substring(5, 7)) - 1
                val limDay = Integer.parseInt(endDate.substring(8))
                limCalendar.set(Calendar.YEAR, limYear)
                limCalendar.set(Calendar.MONTH, limMonth)
                limCalendar.set(Calendar.DAY_OF_MONTH, limDay)
            }

            datePickerDialog.datePicker.maxDate = limCalendar.timeInMillis
            datePickerDialog.show()
        }

        mEndDateText?.keyListener = null
        mEndDateText?.setOnClickListener {
            val currDate = mStartDateText!!.text
            val currCalendar = Calendar.getInstance()
            val currYear = if (currDate!!.isNotBlank()) Integer.parseInt(currDate.substring(0, 4)) else currCalendar.get(Calendar.YEAR)
            val currMonth = if (currDate.isNotBlank()) Integer.parseInt(currDate.substring(5, 7)) - 1 else currCalendar.get(Calendar.MONTH)
            val currDay = if (currDate.isNotBlank()) Integer.parseInt(currDate.substring(8)) else currCalendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        val datePicked = year.toString() + "-" + (if (monthOfYear + 1 < 10) "0" +
                                (monthOfYear + 1) else monthOfYear + 1) + "-" +
                                if (dayOfMonth < 10) "0" + dayOfMonth else dayOfMonth
                        mEndDateText?.setText(datePicked)
                    }, currYear, currMonth, currDay)

            val startDate = mStartDateText?.text
            val limCalendar = Calendar.getInstance()
            if (startDate!!.isNotBlank()) {
                val limYear = Integer.parseInt(startDate.substring(0, 4))
                val limMonth = Integer.parseInt(startDate.substring(5, 7)) - 1
                val limDay = Integer.parseInt(startDate.substring(8))
                limCalendar.set(Calendar.YEAR, limYear)
                limCalendar.set(Calendar.MONTH, limMonth)
                limCalendar.set(Calendar.DAY_OF_MONTH, limDay)
            }

            datePickerDialog.datePicker.minDate = limCalendar.timeInMillis
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
        }
    }

    fun createListStatusProduct(){
        dataOrderHistory.add(OrderHistoryModel("SO300-2", "01 Juni 2018", "On Deliver"))
        dataOrderHistory.add(OrderHistoryModel("SO300-1", "21 Mar 2018", "Approved"))
        dataOrderHistory.add(OrderHistoryModel("SO232-3", "12 Nov 2013", "Rejected"))
        dataOrderHistory.add(OrderHistoryModel("SO121-4", "03 Jan 2012", "Done"))

        for(enttitas in dataOrderHistory){
            //layout list horizontal
            val subparent = LinearLayout(this@ActivityOrderHistory)
            subparent.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            subparent.setOrientation(LinearLayout.HORIZONTAL)
            subparent.setWeightSum(1f)
            subparent.setPadding(0, 40, 0, 40)
            subparent.setTag(enttitas)

            //Isi SO No.
            val subparent1 = LinearLayout(this@ActivityOrderHistory)
            subparent1.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.25f)
            subparent1.setOrientation(LinearLayout.VERTICAL)

            val subparenttv1 = TextView(this@ActivityOrderHistory)
            subparenttv1.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv1.setText(""+enttitas.mSoNo)

            subparent1.addView(subparenttv1)

            //Isi SO Date
            val subparent2 = LinearLayout(this@ActivityOrderHistory)
            subparent2.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f)
            subparent2.setOrientation(LinearLayout.VERTICAL)

            val subparenttv2 = TextView(this@ActivityOrderHistory)
            subparenttv2.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv2.setText(""+enttitas.mSoDate)

            subparent2.addView(subparenttv2)

            //Isi Status
            val subparent3 = LinearLayout(this@ActivityOrderHistory)
            subparent3.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f)
            subparent3.setOrientation(LinearLayout.VERTICAL)

            val subparenttv3 = TextView(this@ActivityOrderHistory)
            subparenttv3.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv3.setText(""+enttitas.mStatus)

            subparent3.addView(subparenttv3)

            //Isi Status Image
            val subparent4 = LinearLayout(this@ActivityOrderHistory)
            subparent4.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.5f)
            subparent4.setOrientation(LinearLayout.VERTICAL)

            val subparenttv4 = ImageView(this@ActivityOrderHistory)
            subparenttv4.setImageResource(R.drawable.icon_view)
            subparenttv4.setLayoutParams(LinearLayout.LayoutParams(100,54))

            subparent4.addView(subparenttv4)

            subparent.addView(subparent1)
            subparent.addView(subparent2)
            subparent.addView(subparent3)
            subparent.addView(subparent4)

            subparent.setOnClickListener(object: View.OnClickListener{
                override fun onClick(p0: View?) {
                    ActivityDetailOrderProduct.launchIntent(this@ActivityOrderHistory, ""+enttitas.mSoNo, ""+enttitas.mSoDate, ""+enttitas.mStatus)
                }
            })

            val v1 = View(this@ActivityOrderHistory)
            v1.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2)
            v1.setBackgroundColor(getResources().getColor(R.color.colorGray))
            v1.setTag(enttitas)

            order_history_layout_item.addView(subparent)
            order_history_layout_item.addView(v1)
        }
    }

}