package com.sparepartfactorysales.apps.sparepartfactorysales.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageButton
import android.widget.ImageView
import com.sparepartfactorysales.apps.sparepartfactorysales.R

class GridViewCatalogOrderCategoryAdapter(c: Context, image: Array<Int>, listener: GridViewAdapterListener): BaseAdapter() {

    private var context = c
    private var mImage = image

    val onClickListeners: GridViewAdapterListener = listener
    interface GridViewAdapterListener{
        fun getData(image: Int)
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        var pSatu = p1
        if(pSatu == null){
            //var pSatu = p1
            /*imageView = ImageView(context)
            val paramLayout = LinearLayout.LayoutParams(100, 100)
            imageView.layoutParams = paramLayout
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            imageView.setPadding(5, 5, 5, 5)*/

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            pSatu = inflater.inflate(R.layout.item_catalog_order_category, p2, false) as View

        }

        val i = pSatu.findViewById(R.id.image_catalog_order_category_gridview) as ImageButton

        i.setImageResource(mImage[p0])
        i.tag
        i.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                onClickListeners!!.getData(mImage.get(p0))
            }

        })


        return pSatu
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return mImage.size
    }
}