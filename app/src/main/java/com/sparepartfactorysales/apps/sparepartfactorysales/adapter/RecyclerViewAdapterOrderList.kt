package com.sparepartfactorysales.apps.sparepartfactorysales.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.sparepartfactorysales.apps.sparepartfactorysales.R
import com.sparepartfactorysales.apps.sparepartfactorysales.model.RecyclerViewModelOrder
import kotlinx.android.synthetic.main.order_item_list.view.*

class RecyclerViewAdapterOrderList(var inputData: ArrayList<RecyclerViewModelOrder>, val context: Context, val listener: RecyclerViewAdapterListener): RecyclerView.Adapter<RecyclerViewAdapterOrderList.ViewHolder>(){

    val onClickListeners: RecyclerViewAdapterListener = listener
    interface RecyclerViewAdapterListener{
        fun getData()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        // membuat view baru
        val v: View = LayoutInflater.from(p0.getContext()).inflate(R.layout.order_item_list, p0, false)
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        val vh = ViewHolder(v)

        return vh
    }

    override fun getItemCount(): Int {
        return inputData.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        p0.itemView.order_item_customer.text = inputData.get(p1).mCustomer
        p0.itemView.order_item_category.text = inputData.get(p1).mCategory
        p0.itemView.order_item_car_merk.text = inputData.get(p1).mCarMerks
        p0.itemView.order_item_oem_no.text = inputData.get(p1).mOemNo
        p0.itemView.order_item_type.text = inputData.get(p1).mtype
        p0.itemView.order_item_quantity.text = inputData.get(p1).mquantity
        p0.itemView.order_item_total_price.text = inputData.get(p1).mTotalPrice
        p0.itemView.image_order.setImageResource(inputData.get(p1).mImage!!)
        p0.itemView.order_item_button_edit.tag
        p0.itemView.order_item_button_edit.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {

            }

        })
        p0.itemView.order_item_button_delete.tag
        p0.itemView.order_item_button_delete.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {

            }

        })
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}