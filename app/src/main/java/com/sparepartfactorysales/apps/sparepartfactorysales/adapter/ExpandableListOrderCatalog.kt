package com.sparepartfactorysales.apps.sparepartfactorysales.adapter

import android.content.Context
import android.database.DataSetObserver
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.sparepartfactorysales.apps.sparepartfactorysales.R

class ExpandableListOrderCatalog(private val mContext: Context, private val mlistDataHeader: ArrayList<String>, private val mlistDataChild: HashMap<String, List<String>>, listener: ExpandableListviewAdapterListener): BaseExpandableListAdapter() {
    /*var mListDataHeader = listDataHeader
    var mListDataChild = listDataChild
    var mContext = context*/

    var mIndexInput: Int = 0
    var maxStock: Int = 20

    val onClickListeners: ExpandableListviewAdapterListener = listener
    interface ExpandableListviewAdapterListener{
        fun getDataKurang(data: String)
        fun getDataTambah(data: String)
    }

    override fun getGroup(groupPosition: Int): Any {
        return this.mlistDataHeader.get(groupPosition)
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(p0: Int, isExpanded: Boolean, p1: View?, p2: ViewGroup): View? {
        val headerTitle = getGroup(p0) as String
        var mView = p1
        if(mView == null){
            val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            mView = inflater.inflate(R.layout.item_done_order_catalog_parent, null)
        }

        val titleDropDown = mView!!.findViewById(R.id.done_order_catalog_dropdownlist) as TextView
        titleDropDown.text = headerTitle


        return mView
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return this.mlistDataChild.get(this.mlistDataHeader.get(groupPosition))!!.size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return this.mlistDataChild.get(this.mlistDataHeader.get(groupPosition))!!.get(childPosition)
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View? {
        val childText = getChild(groupPosition, childPosition) as String
        var mView = convertView
        if(mView == null){
            val inflater = mContext!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            mView = inflater.inflate(R.layout.item_done_order_catalog_child, null)
        }

        val tipeProduk = mView!!.findViewById(R.id.kode_produk) as TextView
        /*val kode = mView.findViewById(R.id.done_order_catalog_dropdownlist) as TextView
        val namaProduk = mView.findViewById(R.id.done_order_catalog_dropdownlist) as TextView
        val jumlahProduk = mView.findViewById(R.id.done_order_catalog_dropdownlist) as TextView
        val hargaProduk = mView.findViewById(R.id.done_order_catalog_dropdownlist) as TextView
        val stokProduk = mView.findViewById(R.id.done_order_catalog_dropdownlist) as TextView*/
        val tambahJumlahProduk = mView!!.findViewById(R.id.button_tambah_jumlah_produk) as ImageButton
        val kurangJumlahProduk = mView!!.findViewById(R.id.button_kurang_jumlah_produk) as ImageButton
        val inputStokProduk = mView.findViewById(R.id.input_stok_produk) as EditText

        tipeProduk.text = childText
        /*kode.text = childText
        namaProduk.text = childText
        jumlahProduk.text = childText
        hargaProduk.text = childText
        stokProduk.text = childText*/
        tambahJumlahProduk.tag
        tambahJumlahProduk.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                if(mIndexInput > 19) {

                }else{
                    mIndexInput++
                    inputStokProduk.setText(mIndexInput.toString())
                    onClickListeners!!.getDataKurang(childText)
                }
            }

        })
        kurangJumlahProduk.tag
        kurangJumlahProduk.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                if(mIndexInput < 1 ) {

                }else{
                    mIndexInput--
                    inputStokProduk.setText(mIndexInput.toString())
                    onClickListeners!!.getDataTambah(childText)
                }
            }

        })

        return mView
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return this.mlistDataHeader.size
    }


}