package com.sparepartfactorysales.apps.sparepartfactorysales.adapter

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sparepartfactorysales.apps.sparepartfactorysales.R
import com.sparepartfactorysales.apps.sparepartfactorysales.model.RecyclerViewModelAllPromo
import kotlinx.android.synthetic.main.allpromo_list_item.view.*

class RecyclerViewAllPromoAdapter(var getData: ArrayList<RecyclerViewModelAllPromo>, val context: Context, val listener: RecyclerViewAdapterListener): RecyclerView.Adapter<RecyclerViewAllPromoAdapter.ViewHolder>(){

    val onClickListeners: RecyclerViewAdapterListener = listener
    interface RecyclerViewAdapterListener{
        fun getData()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        // membuat view baru
        val v: View = LayoutInflater.from(p0.getContext()).inflate(R.layout.allpromo_list_item, p0, false)
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        val vh = ViewHolder(v)

        return vh
    }

    override fun getItemCount(): Int {
        return getData.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        p0.itemView.title_list_item_allpromo.text = getData.get(p1).mTitlePromo
        p0.itemView.date_list_item_allpromo.text = getData.get(p1).mDatePromo
        p0.itemView.timer_list_item_allpromo.text = getData.get(p1).mTimePromo
        p0.itemView.layout_allpromo_list_item.tag
        p0.itemView.layout_allpromo_list_item.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {

            }

        })
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}