package com.sparepartfactorysales.apps.sparepartfactorysales.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.sparepartfactorysales.apps.sparepartfactorysales.R

class GridViewMenuAdapter(c: Context): BaseAdapter(){

    private var context = c

    private val idGambar = arrayOf<Int>(R.drawable.menu_sales_route,
                                        R.drawable.menu_catalog,
                                        R.drawable.menu_cust_info,
                                        R.drawable.menu_marketing_info)

    private val labelGambar = arrayOf("Sales Route",
            "Catalog",
            "Customer Information",
            "Marketing Information")



    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        //var imageView: ImageView? = null
        var pSatu = p1
        if(pSatu == null){
            //var pSatu = p1
            /*imageView = ImageView(context)
            val paramLayout = LinearLayout.LayoutParams(100, 100)
            imageView.layoutParams = paramLayout
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            imageView.setPadding(5, 5, 5, 5)*/

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            pSatu = inflater.inflate(R.layout.item_home_menu_gridview, p2, false) as View

        }
        val t = pSatu!!.findViewById(R.id.label_gridview) as TextView
        val i = pSatu.findViewById(R.id.image_gridview) as ImageView


        i.setImageResource(idGambar[p0])
        t.text = labelGambar[p0]

        /*else{
            //imageView = (p1 as ImageView)
        }*/

        //imageView.setImageResource(idGambar[p0])

        return pSatu
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return idGambar.size
    }
}