package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_sales_route.*
import kotlinx.android.synthetic.main.activity_survey.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*


class ActivitySurvey: AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {
        when(p0){
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
            survey_button_submit ->{
                ActivityCheckOut.launchIntent(this)
            }
        }
    }

    companion object {
        val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivitySurvey::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey)
        toolbar_survey.title_toolbar.text = "SURVEY"
        setClickListener()
    }

    fun setClickListener(){
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
        survey_button_submit.setOnClickListener(this)
    }
}