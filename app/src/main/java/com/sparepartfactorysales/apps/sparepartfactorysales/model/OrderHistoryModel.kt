package com.sparepartfactorysales.apps.sparepartfactorysales.model

class OrderHistoryModel(soNo: String?, soDate: String?, status: String?){

    val mSoNo = soNo
    val mSoDate = soDate
    val mStatus = status
}