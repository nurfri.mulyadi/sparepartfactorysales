package com.sparepartfactorysales.apps.sparepartfactorysales.util;

import android.text.Editable;
import android.text.TextWatcher;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by rifqi on Jun 02, 2016.
 */
public class CurrencyTextWatcher implements TextWatcher {
    private boolean mEditing;
    private boolean mUseSymbol = true;

    public CurrencyTextWatcher() { }

    public CurrencyTextWatcher(boolean useSymbol) {
        mUseSymbol = useSymbol;
    }

    public synchronized void afterTextChanged(Editable s) {
        if(!mEditing) {
            mEditing = true;

            String original = s.toString();
            String formatted = "0";
            String currencyCode = NumberFormat.getCurrencyInstance(new Locale("id", "ID")).getCurrency().getSymbol(new Locale("id", "ID"));
            String digits;

            // attempt to parse exponent
            if (original.contains("E")) {
                try {
                    double amount = Double.parseDouble(original);
                    original = NumberFormat.getCurrencyInstance(new Locale("id", "ID")).format(amount);
                } catch (Exception ignored) {}
            }

            try {
                if (!original.contains(currencyCode)) {
                    original = String.format(Locale.getDefault(), "%s%s", currencyCode, original);
                }
                digits = NumberFormat.getCurrencyInstance(new Locale("id", "ID")).parse(original).toString();
            } catch (ParseException ignored) {
                digits = original.replaceAll("\\D", "");
            }

            if (digits.isEmpty()) digits = "0";

            try {
                formatted = NumberFormat.getCurrencyInstance(new Locale("id", "ID")).format(Double.parseDouble(digits));
            } catch (NumberFormatException ignored) {}

            if (!mUseSymbol) {
                formatted = formatted.replace(currencyCode, "");
            }

            s.replace(0, s.length(), formatted);

            mEditing = false;
        }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    public void onTextChanged(CharSequence s, int start, int before, int count) { }
}

