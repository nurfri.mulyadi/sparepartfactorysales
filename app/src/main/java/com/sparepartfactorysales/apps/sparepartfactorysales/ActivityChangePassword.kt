package com.sparepartfactorysales.apps.sparepartfactorysales

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_change_password.*

class ActivityChangePassword : AppCompatActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when(p0){
            change_button->{
                if(edit_password_new.text.toString().equals(edit_password_new_conf.text.toString(), false)){
                    Toast.makeText(this, "Password Changed!", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "Failed Changed!", Toast.LENGTH_SHORT).show()
                }
            }
            profil_button_back->{
                onBackPressed()
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        setclicklistener()
    }
    fun setclicklistener(){
        change_button.setOnClickListener(this)
        profil_button_back.setOnClickListener(this)
    }
}
