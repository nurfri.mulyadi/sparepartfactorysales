package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_check_out.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.text.SimpleDateFormat
import java.util.*

class ActivityCheckOut: AppCompatActivity(), View.OnClickListener, LocationListener, OnMapReadyCallback {

    companion object {
        val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityCheckOut::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }
    }

    override fun onClick(p0: View?) {
        when(p0){
            check_out_button ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    //countdouwn timer
    var cTimer: CountDownTimer? = null

    //map
    var locationManager: LocationManager? = null
    private var mMap: GoogleMap? = null
    var lat: Double? = null
    var long: Double? = null
    var Nama: String? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_out)

        toolbar_check_out.title_toolbar.text = "CHECK OUT"

        //Inisialisasi Fragment Map===================================
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.check_out_map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this@ActivityCheckOut)
        //===========================================================================================

        lat = -6.224500
        long = 106.840374
        Nama = "Wisma Staco"

        date()
        timer()

        setClickListener()
    }

    fun setClickListener(){
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
        check_out_button.setOnClickListener(this)
    }

    fun date(){
        val calendarMonth = Calendar.getInstance()
        val month_date = SimpleDateFormat("MMMM")
        calendarMonth.set(Calendar.MONTH, 1)
        val month_name = month_date.format(calendarMonth.getTime())

        val calendarDay = Calendar.getInstance()
        val day_date = SimpleDateFormat("EEEE")
        calendarDay.set(Calendar.DAY_OF_WEEK, 1)
        val day_name = day_date.format(calendarDay.getTime())

        val calendarDate = Calendar.getInstance()
        val date = SimpleDateFormat("d")
        val date_name = date.format(calendarDate.getTime())

        val calendarYear = Calendar.getInstance()
        val year_date = SimpleDateFormat("yyyy")
        val year_name = year_date.format(calendarYear.getTime())

        val resultDate = day_name+", "+month_name+" "+date_name+" "+year_name
        check_out_result_date.text = resultDate
    }

    fun timer(){
        var final_timer_result: String? = null
        val timer = Calendar.getInstance()
        val timer_date = SimpleDateFormat("hh:mm aaa")
        val result_timer = timer_date.format(timer.getTime())
        final_timer_result = result_timer.replace("p.m.", "PM").replace("a.m.", "AM")
        check_out_timer.text = final_timer_result

        cTimer = object : CountDownTimer(5000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                Log.d("timer", "seconds remaining: " + millisUntilFinished / 1000)
            }

            override fun onFinish() {
                timer()
            }
        }
        (cTimer as CountDownTimer).start()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        cTimer!!.cancel()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        enableMyLocation()

        val lat = this.lat
        val lon = this.long
        val sydney = LatLng(lat!!, lon!!)
        mMap!!.addMarker(MarkerOptions().position(sydney).title("Marker in $Nama"))
        val zoomLevel = 17.0f

        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel))
        mMap!!.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
            override fun onMarkerClick(marker: Marker): Boolean {
                return false
            }
        })
    }

    //===================================Method enableMyLocation===========================================
    //mendapatkan hak akses permission untuk mendapatkan lokasi user (Untuk di atas API 23)
    fun enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap!!.setMyLocationEnabled(true)
        } else {

        }
    }

    override fun onLocationChanged(location: Location?) {

    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }

}