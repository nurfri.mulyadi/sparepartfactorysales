package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import com.sparepartfactorysales.apps.sparepartfactorysales.util.CurrencyTextWatcher
import kotlinx.android.synthetic.main.activity_collection.*
import java.text.SimpleDateFormat
import java.util.*
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_sales_route.*
import kotlinx.android.synthetic.main.toolbar.view.*


class ActivityCollection: AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {
        when (p0) {
            toolbar_button_home -> {
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back -> {
                onBackPressed()
            }
            collection_button_confirm -> {
                finish()
            }
        }
    }


    companion object {
        /*val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityCollection::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }*/
    }
    fun setclicklistener(){
        toolbar_collection.toolbar_button_back.setOnClickListener(this)
        collection_button_confirm.setOnClickListener(this)
    }

    var cTimer: CountDownTimer? = null
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collection)

        toolbar_collection.title_toolbar.text = "COLLECTION"

        setclicklistener()

        //tanggal
        val calendarMonth = Calendar.getInstance()
        val month_date = SimpleDateFormat("MMMM",  Locale.US)
        calendarMonth.set(Calendar.MONTH, 1)
        val month_name = month_date.format(calendarMonth.getTime())

        val calendarDay = Calendar.getInstance()
        val day_date = SimpleDateFormat("EEEE", Locale.US)
        val day_name = day_date.format(calendarDay.getTime())

        val calendarDate = Calendar.getInstance()
        val date = SimpleDateFormat("d")
        val date_name = date.format(calendarDate.getTime())

        val calendarYear = Calendar.getInstance()
        val year_date = SimpleDateFormat("yyyy")
        val year_name = year_date.format(calendarYear.getTime())

        val resultDate = day_name+", "+month_name+" "+date_name+" "+year_name
        collection_result_date.text = resultDate

        //time
        timer()

        //format input Rp
        collection_input_amount.setRawInputType(InputType.TYPE_CLASS_NUMBER)
        collection_input_amount.addTextChangedListener(CurrencyTextWatcher())

        setClickListener()
    }

    fun setClickListener(){
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
        collection_button_confirm.setOnClickListener(this)
    }

    fun timer(){
        var final_timer_result: String? = null
        val timer = Calendar.getInstance()
        val timer_date = SimpleDateFormat("hh:mm aaa")
        val result_timer = timer_date.format(timer.getTime())
        final_timer_result = result_timer.replace("p.m.", "PM").replace("a.m.", "AM")
        collection_timer.text = final_timer_result

        cTimer = object : CountDownTimer(10000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                Log.d("timer", "seconds remaining: " + millisUntilFinished / 1000)
            }

            override fun onFinish() {
                timer()
            }
        }
        (cTimer as CountDownTimer).start()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        cTimer!!.cancel()
    }
}