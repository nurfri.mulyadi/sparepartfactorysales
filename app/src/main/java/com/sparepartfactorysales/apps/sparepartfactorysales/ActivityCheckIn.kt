package com.sparepartfactorysales.apps.sparepartfactorysales

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import kotlinx.android.synthetic.main.activity_check_in.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.text.SimpleDateFormat
import java.util.*
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.location.LocationManager
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import android.content.IntentFilter
import android.app.PendingIntent
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.nfc.*
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import android.os.*
import android.os.Environment.getExternalStorageDirectory
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.toolbar.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.nio.charset.Charset
import kotlin.experimental.and


class ActivityCheckIn: AppCompatActivity(), LocationListener, OnMapReadyCallback, View.OnClickListener {

    override fun onClick(p0: View?) {
        when(p0){
            check_in_button_scan_nfc ->{
                progres = ProgressDialog.show(this, null, "PLEASE WAIT...", true, false)
                if(mAdapter != null && mAdapter!!.isEnabled) {
                    mAdapter!!.enableForegroundDispatch(this, mPI, mFilter, null)
                }else{
                    progres!!.dismiss()
                    Toast.makeText(this, "NFC Not Available in Device", Toast.LENGTH_SHORT).show()
                }
            }
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
            check_in_button_take_photo ->{
                if (checkCameraPermission()){
                    /*val imageIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(imageIntent, CAMERA_PIC_REQUEST)*/

                    val imageIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
                    timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())

                    //folder
                    val imagesFolder = File(getExternalStorageDirectory(), "SparePartFactorySalesImages")
                    imagesFolder.mkdirs()

                    //file name
                    val image = File(imagesFolder, "QR_$timeStamp.png")
                    val uriSavedImage = Uri.fromFile(image)

                    imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage)
                    startActivityForResult(imageIntent, CAMERA_PIC_REQUEST)
                }
            }
            check_in_button_retake_photo ->{
                if (checkCameraPermission()){
                    /*val imageIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(imageIntent, CAMERA_PIC_REQUEST)*/
                    //camera
                    val imageIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
                    timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())

                    //folder
                    val imagesFolder = File(getExternalStorageDirectory(), "SparePartFactorySalesImages")

                    //file name
                    val image = File(imagesFolder, "QR_$timeStamp.png")
                    val uriSavedImage = Uri.fromFile(image)

                    imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage)
                    startActivityForResult(imageIntent, CAMERA_PIC_REQUEST)
                }
            }
            check_in_button ->{
                    ActivityCatalog.launchIntent(this, "checkin")
            }
        }
    }

    companion object {
       /* val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityCheckIn::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }*/
    }

    var timeStamp: String? = null
    val CAMERA_PIC_REQUEST = 1337
    var progres: ProgressDialog? = null
    var locationManager: LocationManager? = null
    private var mMap: GoogleMap? = null
    var cTimer: CountDownTimer? = null
    var lat: Double? = null
    var long: Double? = null
    var Nama: String? = null

    //variabel NFC
    var mAdapter: NfcAdapter? = null
    var nfcManager: NfcManager? = null
    var mTag: Tag? = null
    var mPI: PendingIntent? = null
    var mFilter: Array<IntentFilter>? = null
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_in)

        toolbar_check_in.title_toolbar.text = "CHECK IN"

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.check_in_map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this@ActivityCheckIn)

        lat = -6.224500
        long = 106.840374
        Nama = "Wisma Staco"

        //tanggal & timer
        date()
        timer()

        //Inisialisasi Variabel PendingIntent dan IntentFilter NFC===================================
        nfcManager = this.getSystemService(Context.NFC_SERVICE) as NfcManager?
        mAdapter = nfcManager!!.getDefaultAdapter()
        mPI = PendingIntent.getActivity(applicationContext, 0,
                Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0)
        val tagDetected = IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED)
        val filter2 = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
        mFilter = arrayOf(tagDetected, filter2)
        //===========================================================================================

        setClickLIstener()
    }

    fun setClickLIstener(){
        check_in_button_scan_nfc.setOnClickListener(this)
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
        check_in_button_take_photo.setOnClickListener(this)
        check_in_button_retake_photo.setOnClickListener(this)
        check_in_button.setOnClickListener(this)
    }

    fun date(){
        val calendarMonth = Calendar.getInstance()
        val month_date = SimpleDateFormat("MMMM")
        calendarMonth.set(Calendar.MONTH, 1)
        val month_name = month_date.format(calendarMonth.getTime())

        val calendarDay = Calendar.getInstance()
        val day_date = SimpleDateFormat("EEEE")
        calendarDay.set(Calendar.DAY_OF_WEEK, 1)
        val day_name = day_date.format(calendarDay.getTime())

        val calendarDate = Calendar.getInstance()
        val date = SimpleDateFormat("d")
        val date_name = date.format(calendarDate.getTime())

        val calendarYear = Calendar.getInstance()
        val year_date = SimpleDateFormat("yyyy")
        val year_name = year_date.format(calendarYear.getTime())

        val resultDate = day_name+", "+month_name+" "+date_name+" "+year_name
        check_in_result_date.text = resultDate
    }

    fun timer(){
        var final_timer_result: String? = null
        val timer = Calendar.getInstance()
        val timer_date = SimpleDateFormat("hh:mm aaa")
        val result_timer = timer_date.format(timer.getTime())
        final_timer_result = result_timer.replace("p.m.", "PM").replace("a.m.", "AM")
        check_in_timer.text = final_timer_result

        cTimer = object : CountDownTimer(5000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                Log.d("timer", "seconds remaining: " + millisUntilFinished / 1000)
            }

            override fun onFinish() {
                timer()
            }
        }
        (cTimer as CountDownTimer).start()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        cTimer!!.cancel()
    }

    //Configuratioin Map
    override fun onLocationChanged(location: Location?) {

    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        enableMyLocation()

        val lat = this.lat
        val lon = this.long
        val sydney = LatLng(lat!!, lon!!)
        mMap!!.addMarker(MarkerOptions().position(sydney).title("Marker in $Nama"))
        val zoomLevel = 17.0f

        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel))
        mMap!!.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
            override fun onMarkerClick(marker: Marker): Boolean {
                return false
            }
        })
    }

    //===================================Method enableMyLocation===========================================
    //mendapatkan hak akses permission untuk mendapatkan lokasi user (Untuk di atas API 23)
    fun enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap!!.setMyLocationEnabled(true)
        } else {

        }
    }

    fun getNdefMessage(intent: Intent): Array<NdefMessage?>? {
        var msgs: Array<NdefMessage?>? = null
        val rawMsgs : Array<Parcelable>? = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
        if (rawMsgs != null) {
            msgs = arrayOfNulls(rawMsgs.size)
            for (i in 0 until rawMsgs.size) {
                msgs[i] = rawMsgs[i] as NdefMessage
            }
        }
        return msgs
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        if (intent!!.getAction().equals(NfcAdapter.ACTION_NDEF_DISCOVERED)) {
            Toast.makeText(getApplicationContext(), "Ndefdiscovered", Toast.LENGTH_SHORT).show()
        }else if(intent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)){
            mTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG)
            progres!!.dismiss()
            Toast.makeText(getApplicationContext(), "Smartcard detected", Toast.LENGTH_SHORT).show()
            //hiden button nfc
            check_in_button_scan_nfc.visibility = View.GONE
            check_in_button_take_photo.visibility = View.VISIBLE

            val messages = getNdefMessage(intent)
            if (messages == null) {
                Toast.makeText(getApplicationContext(), "Data di dalam kartu kosong", Toast.LENGTH_SHORT).show();
                return
            }

            try {
                val payload2: ByteArray? = messages[0]!!.getRecords()[0].getPayload()
                val byte: Byte = 0
                val textEncoding2: String = if (payload2!![0] and 128.toByte() == byte) "UTF-8" else "UTF-16"

                val charset: Charset = Charsets.UTF_8
                val charsets: Charset = Charsets.US_ASCII

                //Get the Language Code
                val languageCodeLength2 = payload2!![0] and 63
                val languageCode = String(payload2, 1, languageCodeLength2.toInt(), charsets)
                val userData2 = String(payload2, languageCodeLength2 + 1, payload2.size - languageCodeLength2 - 1, charset)
                Toast.makeText(this@ActivityCheckIn, "Hasil: "+userData2, Toast.LENGTH_SHORT).show()

            } catch (e: Exception) {
                println("error: "+e.toString())
            }
        }else {
            Toast.makeText(getApplicationContext(), "Undefined smartcard", Toast.LENGTH_SHORT).show()
        }
    }

    //Camera Permission
    fun checkCameraPermission(): Boolean{
        if (Build.VERSION.SDK_INT >= 23) {
            if((checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)  == PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)  == PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(android.Manifest.permission.CAMERA)  == PackageManager.PERMISSION_GRANTED)){
                Log.v("checkPermission","Permission is granted")
                return true
            }else{
                ActivityCompat.requestPermissions(
                        this, arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        20
                )
                return false
            }
        }else { //permission is automatically granted on sdk<23 upon installation
            Log.v("asss","Permission is granted")
            return true
        }
    }

    //ambil hasil capture
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
            check_in_result_photo.setImageResource(android.R.color.transparent)
            check_in_button_take_photo.visibility = View.GONE
            check_in_sublayout_photo.visibility = View.VISIBLE
            check_in_button_retake_photo.visibility = View.VISIBLE
            check_in_button.isEnabled = true
            check_in_button.setBackgroundColor(getResources().getColor(R.color.colorYellowTextButton))

            /*val image = data!!.getExtras().get("data") as Bitmap
            check_in_result_photo.setImageBitmap(image)*/
            val out = File(Environment.getExternalStorageDirectory().toString() + "/SparePartFactorySalesImages/" + "QR_$timeStamp.png")
            val  filePath = out.absolutePath
            val bitmap = BitmapFactory.decodeFile(filePath)
            val exif = ExifInterface(filePath)
            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)

            var rotatedBitmap: Bitmap? = null
            when(orientation){
                ExifInterface.ORIENTATION_ROTATE_90 ->{
                    rotatedBitmap = rotateImage(bitmap, 90f)
                }
                ExifInterface.ORIENTATION_ROTATE_180 ->{
                    rotatedBitmap = rotateImage(bitmap, 180f)
                }
                ExifInterface.ORIENTATION_ROTATE_270 ->{
                    rotatedBitmap = rotateImage(bitmap, 270f)
                }
                ExifInterface.ORIENTATION_NORMAL ->{
                    rotatedBitmap = bitmap
                }
            }

            try {
                try{
                    //save images rotate
                    out.createNewFile()
                    val outputStream = FileOutputStream(out)
                    rotatedBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream)
                }catch(e: Exception){
                    Toast.makeText(this, "Error Save Image: "+e.toString(), Toast.LENGTH_SHORT).show()
                }finally {
                    //resize image
                    saveBitmapToFile(out)
                }
            }catch (e: Exception) {
                Toast.makeText(this, "Error Resize: "+e.toString(), Toast.LENGTH_SHORT).show()
            }finally {
                //val mBitmap = BitmapFactory.decodeFile(out.absolutePath)
                check_in_result_photo.setImageBitmap(rotatedBitmap)
            }
        }

    }

    /*override fun onPause() {
        // TODO Auto-generated method stub
        super.onPause()
        mAdapter!!.disableForegroundDispatch(this)
    }*/

    /*override fun onResume() {
        // TODO Auto-generated method stub
        super.onResume()
        mAdapter!!.enableForegroundDispatch(this, mPI, mFilter, null)
    }*/

    //rotate Image
    fun rotateImage(source: Bitmap, angle: Float): Bitmap{
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }

    //resize image
    fun saveBitmapToFile(file: File): File{
        try{
            // BitmapFactory options to downsize the image
            val o: BitmapFactory.Options  = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            o.inSampleSize = 6

            var inputStream = FileInputStream(file)
            //Bitmap selectedBitmap = null
            BitmapFactory.decodeStream(inputStream, null, o)
            inputStream.close()

            // The new size we want to scale to
            val REQUIRED_SIZE = 75

            var scale = 1
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            val o2: BitmapFactory.Options  = BitmapFactory.Options()
            o2.inSampleSize = scale
            inputStream = FileInputStream(file)

            val selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2)
            inputStream.close()

            // here i override the original image file
            file.createNewFile()
            val outputStream = FileOutputStream(file)

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream)
        }catch(e: Exception){
            println("Error: "+e.toString())
        }
        return file
    }
}