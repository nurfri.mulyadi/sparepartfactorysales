package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.ProductKnowledgeListItemAdapter
import kotlinx.android.synthetic.main.activity_product_knowledge_list_item.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import kotlin.properties.Delegates

class ActivityProductKnwoledgeListItem: AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {
        when(p0){
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    companion object {
        val REQUEST_CODE = 15
        var mImage: Int? = null
        var mFlag: String? = null
        fun launchIntent(caller: Activity?, image: Int, flag: String?){
            val intent = Intent(caller, ActivityProductKnwoledgeListItem::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)

            mImage = image
            mFlag = flag
        }

        fun launchIntentNews(caller: Activity?, flag: String?){
            val intent = Intent(caller, ActivityProductKnwoledgeListItem::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)

            mFlag = flag
        }
    }

    val dataProductKnowledgeListItem = ArrayList<String>()
    var adapter by Delegates.notNull<ProductKnowledgeListItemAdapter>()
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_knowledge_list_item)

        toolbar_product_knowledge_list_item.title_toolbar.text = "PRODUCT KNOWLEDGE"

        setClickListener()

        dataProductKnowledgeListItem.add("BRAKE MASTER(MASTER REM)")
        dataProductKnowledgeListItem.add("BRAKE BOOSTER")
        dataProductKnowledgeListItem.add("BRAKE PAD")
        dataProductKnowledgeListItem.add("WATER PUMP")
        dataProductKnowledgeListItem.add("DISC BRAKE")
        dataProductKnowledgeListItem.add("BRAKE SHOE, HAND BRAKE SHOW")


        adapter = ProductKnowledgeListItemAdapter(dataProductKnowledgeListItem, this@ActivityProductKnwoledgeListItem, object: ProductKnowledgeListItemAdapter.RecyclerViewAdapterListener{
            override fun getData() {
                ActivityDescriptionProduct.launchIntent(this@ActivityProductKnwoledgeListItem)
            }

        })
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this@ActivityProductKnwoledgeListItem)
        product_knowledge_list_item_recyclerview.layoutManager = layoutManager
        product_knowledge_list_item_recyclerview.adapter = adapter

        if(mFlag!!.contains("brand")) {
            product_knowledge_list_item_image_brand.setImageResource(mImage!!)
        }else if(mFlag!!.contains("news")){
            product_knowledge_list_item_image_brand.background = resources.getDrawable(R.drawable.button_news)
        }
    }

    fun setClickListener(){
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }
}