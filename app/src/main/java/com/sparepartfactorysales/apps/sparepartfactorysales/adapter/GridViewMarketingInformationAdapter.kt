package com.sparepartfactorysales.apps.sparepartfactorysales.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import com.sparepartfactorysales.apps.sparepartfactorysales.R
import com.sparepartfactorysales.apps.sparepartfactorysales.model.ItemCatalog

class GridViewMarketingInformationAdapter(c: Context, image: ArrayList<ItemCatalog>, listener: GridViewAdapterListener): BaseAdapter(), Filterable {

    private var context = c
    private var mImage = image
    private var mImageOriginal = image
    private var imageFilter: Filter? = null

    val onClickListeners: GridViewAdapterListener = listener
    interface GridViewAdapterListener{
        fun getData(image: Int)
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        var pSatu = p1
        if(pSatu == null){
            //var pSatu = p1
            /*imageView = ImageView(context)
            val paramLayout = LinearLayout.LayoutParams(100, 100)
            imageView.layoutParams = paramLayout
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            imageView.setPadding(5, 5, 5, 5)*/

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            pSatu = inflater.inflate(R.layout.item_marketing_information_menu_gridview, p2, false) as View

        }

        val i = pSatu.findViewById(R.id.image_marketing_information_gridview) as ImageView

        i.setImageResource(mImage[p0].mIdIMG)
        i.tag
        i.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                onClickListeners!!.getData(mImage.get(p0).mIdIMG)
            }

        })


        return pSatu
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return mImage.size
    }

    fun resetData(){
        mImage = mImageOriginal
    }

    override fun getFilter(): Filter {
        if (imageFilter == null)
            imageFilter = ImageFilter()

        return imageFilter!!
    }

    inner class ImageFilter: Filter(){
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val results = FilterResults()
            // We implement here the filter logic
            if(constraint == null || constraint.length == 0){
                //No filter implemented we return all the list
                results.values = mImageOriginal
                results.count = mImageOriginal.size
            }else{
                // We perform filtering operation
                val nIdIMG = ArrayList<ItemCatalog>()
                for (p in mImage) {
                    if (p.mNameIMG.toUpperCase().startsWith(constraint.toString().toUpperCase()))
                        nIdIMG.add(p)
                }
                results.values = nIdIMG
                results.count = nIdIMG.size
            }
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            // Now we have to inform the adapter about the new list filtered
            if(results!!.count == 0){
                notifyDataSetInvalidated()
            }else{
                mImage = results.values as ArrayList<ItemCatalog>
                notifyDataSetChanged()
            }
        }

    }
}