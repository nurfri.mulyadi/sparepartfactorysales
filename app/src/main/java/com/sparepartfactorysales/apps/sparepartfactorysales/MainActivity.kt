package com.sparepartfactorysales.apps.sparepartfactorysales

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import android.telephony.TelephonyManager
import android.util.Log
import com.sparepartfactorysales.apps.sparepartfactorysales.network.ApiService
import com.sparepartfactorysales.apps.sparepartfactorysales.network.BaseApiBuilder
import com.sparepartfactorysales.apps.sparepartfactorysales.network.BaseApiClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.util.*
import javax.security.auth.callback.Callback


class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when(p0){
            login_button ->{
                //ActivityHome.launchIntent(this)
                startActivity(Intent(this@MainActivity, ActivityHome::class.java))
                getApi()
            }
            text_register->{
                enablePermission()
            }
        }
    }
    companion object {

    }
    fun getApi(){
        val postService = BaseApiClient.getClient()
        postService?.getPosts()?.enqueue(object : Callback, retrofit2.Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAG", "ERROR: ${t.message}")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                Log.e("MAIN ACTIVITY", "Response: ${response.body()}")
            }

        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setClickListener()


    }

    fun setClickListener(){
        login_button.setOnClickListener(this)
        text_register.setOnClickListener(this)
    }

    /*getting IMEI or DeviceID from device*/
    fun enablePermission(){
        val  telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED) {
            // Permission is  granted
            val imei : String? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                telephonyManager.imei
            } else { // older OS  versions
                telephonyManager.getDeviceId()
            }

            imei?.let {
                Log.i("Log", "DeviceId=$imei" )
            }
            Toast.makeText(this, "IMEI Registered", Toast.LENGTH_SHORT).show()

        } else {  // Permission is not granted

        }
    }
}
