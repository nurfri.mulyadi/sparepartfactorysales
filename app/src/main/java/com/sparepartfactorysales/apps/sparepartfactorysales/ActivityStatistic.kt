package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_statistic.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import android.R.attr.data
import android.renderscript.Sampler
import com.anychart.AnyChart.cartesian
import com.anychart.AnyChart.column
import com.anychart.AnyChart.cartesian
import android.R.attr.animation
import com.anychart.charts.Pie
import android.widget.Toast
import com.anychart.APIlib
import com.anychart.chart.common.listener.ListenersInterface
import com.anychart.AnyChart.pie
import com.anychart.chart.common.listener.Event
import com.anychart.AnyChart.pie
import com.anychart.enums.*


class ActivityStatistic: AppCompatActivity(), View.OnClickListener {

    companion object {
        val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityStatistic::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }
    }

    var mStartDateText: EditText? = null
    var mEndDateText: EditText? = null
    override fun onClick(p0: View?) {
        when(p0){
            toolbar_button_back ->{
                onBackPressed()
            }

            toolbar_button_home ->{
                onBackPressed()
            }

            statistic_button_search ->{

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistic)

        mStartDateText = statistic_input_start_date as EditText
        mEndDateText = statistic_input_end_date as EditText

        chartStatistic()
        chartStatisticPie()
        dataPicker()
        setClickListener()
    }

    fun setClickListener(){
        toolbar_button_back.setOnClickListener(this)
        toolbar_button_home.setOnClickListener(this)
        statistic_button_search.setOnClickListener(this)
    }

    private fun dataPicker(){
        mStartDateText?.keyListener = null
        mStartDateText?.setOnClickListener {
            val currDate = mStartDateText!!.text
            val currCalendar = Calendar.getInstance()
            val currYear = if (currDate!!.isNotBlank()) Integer.parseInt(currDate.substring(0, 4)) else currCalendar.get(Calendar.YEAR)
            val currMonth = if (currDate.isNotBlank()) Integer.parseInt(currDate.substring(5, 7)) - 1 else currCalendar.get(Calendar.MONTH)
            val currDay = if (currDate.isNotBlank()) Integer.parseInt(currDate.substring(8)) else currCalendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        val datePicked = year.toString() + "-" + (if (monthOfYear + 1 < 10) "0" +
                                (monthOfYear + 1) else monthOfYear + 1) + "-" +
                                if (dayOfMonth < 10) "0" + dayOfMonth else dayOfMonth
                        mStartDateText?.setText(datePicked)
                    }, currYear, currMonth, currDay)

            val endDate = mEndDateText?.text
            val limCalendar = Calendar.getInstance()
            if (endDate!!.isNotBlank()) {
                val limYear = Integer.parseInt(endDate.substring(0, 4))
                val limMonth = Integer.parseInt(endDate.substring(5, 7)) - 1
                val limDay = Integer.parseInt(endDate.substring(8))
                limCalendar.set(Calendar.YEAR, limYear)
                limCalendar.set(Calendar.MONTH, limMonth)
                limCalendar.set(Calendar.DAY_OF_MONTH, limDay)
            }

            datePickerDialog.datePicker.maxDate = limCalendar.timeInMillis
            datePickerDialog.show()
        }

        mEndDateText?.keyListener = null
        mEndDateText?.setOnClickListener {
            val currDate = mStartDateText!!.text
            val currCalendar = Calendar.getInstance()
            val currYear = if (currDate!!.isNotBlank()) Integer.parseInt(currDate.substring(0, 4)) else currCalendar.get(Calendar.YEAR)
            val currMonth = if (currDate.isNotBlank()) Integer.parseInt(currDate.substring(5, 7)) - 1 else currCalendar.get(Calendar.MONTH)
            val currDay = if (currDate.isNotBlank()) Integer.parseInt(currDate.substring(8)) else currCalendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        val datePicked = year.toString() + "-" + (if (monthOfYear + 1 < 10) "0" +
                                (monthOfYear + 1) else monthOfYear + 1) + "-" +
                                if (dayOfMonth < 10) "0" + dayOfMonth else dayOfMonth
                        mEndDateText?.setText(datePicked)
                    }, currYear, currMonth, currDay)

            val startDate = mStartDateText?.text
            val limCalendar = Calendar.getInstance()
            if (startDate!!.isNotBlank()) {
                val limYear = Integer.parseInt(startDate.substring(0, 4))
                val limMonth = Integer.parseInt(startDate.substring(5, 7)) - 1
                val limDay = Integer.parseInt(startDate.substring(8))
                limCalendar.set(Calendar.YEAR, limYear)
                limCalendar.set(Calendar.MONTH, limMonth)
                limCalendar.set(Calendar.DAY_OF_MONTH, limDay)
            }

            datePickerDialog.datePicker.minDate = limCalendar.timeInMillis
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
        }
    }

    fun chartStatistic(){
        APIlib.getInstance().setActiveAnyChartView(any_chart_view)
        val cartesian = AnyChart.column()

        val data = ArrayList<DataEntry>()
        data.add(ValueDataEntry("T. Abadi", 100))
        data.add(ValueDataEntry("T. Bhakti", 15))
        data.add(ValueDataEntry("T. Cemerlang", 13))
        data.add(ValueDataEntry("T. Makmur", 11))
        data.add(ValueDataEntry("T. Bintang", 10))

        val column = cartesian.column(data)
        column.fill("#ffca06")
        column.stroke("#ffca06")

        column.tooltip()
                .titleFormat("{%X}")
                .position(Position.CENTER_BOTTOM)
                .anchor(Anchor.CENTER_BOTTOM)
                .offsetX(0)
                .offsetY(1)
                .format("{%Value}{groupsSeparator: }")
                .fontColor("#ffca06")

        cartesian.animation(true)
        cartesian.title("Top 5 Store by Quantity")

        cartesian.yScale().minimum(0.0)

        cartesian.yAxis(0).labels().format("{%Value}{groupsSeparator: }")

        cartesian.tooltip().positionMode(TooltipPositionMode.POINT)
        cartesian.interactivity().hoverMode(HoverMode.BY_X)

        cartesian.xAxis(0).title("Toko")
        cartesian.yAxis(0).title("Penjualan Produk")

        any_chart_view.setChart(cartesian)

    }

    fun chartStatisticPie(){
        APIlib.getInstance().setActiveAnyChartView(any_chart_view_pie)
        val pie = AnyChart.pie()

        pie.setOnClickListener(object : ListenersInterface.OnClickListener(arrayOf("x", "value")) {
            override fun onClick(event: Event) {
                Toast.makeText(this@ActivityStatistic, event.getData().get("x") + ":" + event.getData().get("value"), Toast.LENGTH_SHORT).show()
            }
        })

        val data1 = ArrayList<DataEntry>()
        data1.add(ValueDataEntry("T. Abadi", 100))
        data1.add(ValueDataEntry("T. Bhakti", 15))
        data1.add(ValueDataEntry("T. Cemerlang", 13))
        data1.add(ValueDataEntry("T. Makmur", 11))
        data1.add(ValueDataEntry("T. Bintang", 10))

        pie.data(data1)
        pie.title("Fruits imported in 2015 (in kg)")

        pie.labels().position("outside")

        pie.legend().title().enabled(true)
        pie.legend().title()
                .text("Retail channels")
                .padding(0.0, 0.0, 10.0, 0.0)

        pie.legend()
                .position("center-bottom")
                .itemsLayout(LegendLayout.HORIZONTAL)
                .align(Align.CENTER)

        any_chart_view_pie.setChart(pie)

    }
}