package com.sparepartfactorysales.apps.sparepartfactorysales.model

class RecyclerViewModelAllPromo(titlePromo: String?, datePromo: String?, timePromo: String?){

    val mTitlePromo = titlePromo
    val mDatePromo = datePromo
    val mTimePromo = timePromo
}