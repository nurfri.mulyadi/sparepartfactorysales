package com.sparepartfactorysales.apps.sparepartfactorysales.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.sparepartfactorysales.apps.sparepartfactorysales.model.Model
import android.widget.TextView
import android.widget.CheckBox
import com.sparepartfactorysales.apps.sparepartfactorysales.R.id.holder
import com.sparepartfactorysales.apps.sparepartfactorysales.R.id.holder
import android.support.design.widget.CoordinatorLayout.Behavior.setTag
import com.sparepartfactorysales.apps.sparepartfactorysales.R.id.animal
import com.sparepartfactorysales.apps.sparepartfactorysales.R.id.cb
import com.sparepartfactorysales.apps.sparepartfactorysales.R.layout.lv_item
import android.support.v4.content.ContextCompat.getSystemService
import android.view.LayoutInflater
import android.widget.Toast
import com.sparepartfactorysales.apps.sparepartfactorysales.R
import com.sparepartfactorysales.apps.sparepartfactorysales.R.id.holder
import com.sparepartfactorysales.apps.sparepartfactorysales.R.id.holder
import com.sparepartfactorysales.apps.sparepartfactorysales.R.id.holder








class CustomAdapter(context: Context?, var modelArrayList: ArrayList<Model> ): BaseAdapter() {

    val context = context

    fun getData(modelArrayList: ArrayList<Model>) {
        this.modelArrayList = modelArrayList
    }

    override fun getViewTypeCount(): Int {
        return count
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var convertView = convertView
        var holder: ViewHolder? = null
        if (convertView == null) {
            holder = ViewHolder()
            val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.lv_item, null, true)

            holder.checkBox = convertView.findViewById(R.id.cb) as CheckBox
            holder.tvAnimal = convertView.findViewById(R.id.animal) as TextView
            //holder.view = convertView.findViewById(R.id.view) as View

            convertView.setTag(holder)
        }else{
            holder = (convertView.tag as ViewHolder)
        }

        holder.tvAnimal!!.text = modelArrayList[position].animal

        holder.checkBox?.isChecked = modelArrayList[position].isSelected

        holder.checkBox!!.setTag(R.integer.btnplusview, convertView)
        holder.checkBox!!.setTag(position)


        holder.checkBox!!.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                val tempview = holder.checkBox!!.getTag(R.integer.btnplusview) as View
                val tv = tempview.findViewById(R.id.animal) as TextView
                val pos = holder.checkBox!!.getTag() as Int
                //Toast.makeText(context, "Checkbox "+pos+" clicked!", Toast.LENGTH_SHORT).show()
                if(modelArrayList.get(pos).isSelected){
                    modelArrayList.get(pos).isSelected = false
                }else {
                    modelArrayList.get(pos).isSelected = true
                }
            }

        })

        return convertView
    }

    override fun getItem(position: Int): Any {
        return modelArrayList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return modelArrayList.size
    }

    private inner class ViewHolder {

        var checkBox: CheckBox? = null
        var tvAnimal: TextView? = null
        //var view: View? = null

    }
}