package com.sparepartfactorysales.apps.sparepartfactorysales.fragment

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.sparepartfactorysales.apps.sparepartfactorysales.R
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.CustomAdapter
import com.sparepartfactorysales.apps.sparepartfactorysales.model.Model
import kotlinx.android.synthetic.main.dialog_fragment_add_sales_route.*
import kotlinx.android.synthetic.main.dialog_fragment_add_sales_route.view.*



class FragmentAddStoreSalesRoute: DialogFragment(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when(p0){
            close_dialog_fragment_add_sales_route ->{
                dialog.dismiss()
            }
            dialog_sales_route_button_add ->{
                getDataListStore()
            }
        }
    }

    var adapter : CustomAdapter? = null
    var mListenerReject: ApprovalActionListenerReject? = null
    var modelArrayList = ArrayList<Model>()
    //var animalist:MutableList<Model> = mutableListOf<Model>()

    val animalist = arrayOf<String>("Toko Sinar Abadi", "Toko Bhakti Guna", "Toko Cemerlang Jaya", "Toko Makmur Jaya", "Toko Bintang Terang", "Toko Sinar Sentosa")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val view : View = inflater.inflate(R.layout.dialog_fragment_add_sales_route, container, false)

        view.close_dialog_fragment_add_sales_route.setOnClickListener(this)
        view.dialog_sales_route_button_add.setOnClickListener(this)

        modelArrayList = getModel(false)

        //create and set adapter to listview
        adapter = CustomAdapter(context, modelArrayList)
        view.listView_search_store.adapter = adapter
        /*view.listView_search_store.onItemClickListener = object : AdapterView.OnItemClickListener{
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                prepareForResultReject(p0!!.getItemAtPosition(p2).toString())
                dismiss()
            }

        }*/

        return view
    }

    fun getModel(isSelect: Boolean): ArrayList<Model>{
        val list = ArrayList<Model>()
        for(value in animalist){
            val model = Model()
            model.isSelected = isSelect
            model.animal = value
            list.add(model)
        }

        return list
    }

    fun getDataListStore(){
        for (i in modelArrayList) {
            if (i.isSelected) {
                println("Hasil Select: "+i.animal)

                prepareForResultReject(i.animal)
                dismiss()
            }
        }
    }

    private fun prepareForResultReject(data: String?) {
        mListenerReject?.onApprovalResultReject(data)

    }

    interface ApprovalActionListenerReject {
        fun onApprovalResultReject(rejectDesc : String?)
    }
}