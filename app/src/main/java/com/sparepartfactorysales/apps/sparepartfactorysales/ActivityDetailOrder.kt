package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.sparepartfactorysales.apps.sparepartfactorysales.model.DetailOrderModel
import kotlinx.android.synthetic.main.activity_detail_order.*
import kotlinx.android.synthetic.main.toolbar_order.*
import kotlinx.android.synthetic.main.toolbar_order.view.*

class ActivityDetailOrder: AppCompatActivity(), View.OnClickListener{
    override fun onClick(p0: View?) {
        when(p0){
            detail_order_button_confirm ->{
                if(mflag!!.contains("else")) {
                    val intent = Intent(applicationContext, ActivityHome::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }else if(mflag!!.contains("checkin")){
                    ActivitySurvey.launchIntent(this)
                }
            }
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_order ->{
                ActivityOrder.launchIntent(this, "else")
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    companion object {
        val REQUEST_CODE = 15
        var mflag: String? = null
        fun launchIntent(caller: Activity?, flag: String?) {
            val intent = Intent(caller, ActivityDetailOrder::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)

            mflag = flag
        }
    }

    val dataDetailOrder = ArrayList<DetailOrderModel>()
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_order)

        toolbar_detail_order.title_toolbar.text = "DETAIL ORDER"

        createListTotalItem()
        setClickListener()
    }

    fun setClickListener(){
        detail_order_button_confirm.setOnClickListener(this)
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_order.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }

    fun createListTotalItem(){
        dataDetailOrder.add(DetailOrderModel("DWF-011", "10", "Pcs", "117.000", "1.170.000", "DELTA(V22)FR RH(1 1/4)"))
        dataDetailOrder.add(DetailOrderModel("HYWR-002", "5", "Pcs", "78.000", "390.000", "ATOZ RR LH(7/10)"))
        dataDetailOrder.add(DetailOrderModel("FDB-003", "5", "Pcs", "530.000", "2.650.000", "RANGER 4x4 2500cc 98-02 FR"))

        for(enttitas in dataDetailOrder){

            //layout list horizontal
            val subparent = LinearLayout(this@ActivityDetailOrder)
            subparent.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            subparent.setOrientation(LinearLayout.HORIZONTAL)
            subparent.setWeightSum(1f)
            subparent.setTag(enttitas)

            //Isi Code
            val subparent1 = LinearLayout(this@ActivityDetailOrder)
            subparent1.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.25f)
            subparent1.setOrientation(LinearLayout.VERTICAL);

            val subparenttv1 = TextView(this@ActivityDetailOrder)
            subparenttv1.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv1.setText(""+enttitas.mCode)
            subparenttv1.setTypeface(subparenttv1.getTypeface(), Typeface.BOLD)

            subparent1.addView(subparenttv1)

            //Isi Qty
            val subparent2 = LinearLayout(this@ActivityDetailOrder)
            subparent2.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.12f)
            subparent2.setOrientation(LinearLayout.VERTICAL)

            val subparenttv2 = TextView(this@ActivityDetailOrder)
            subparenttv2.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv2.setText(""+enttitas.mQty)
            subparenttv2.setTypeface(subparenttv2.getTypeface(), Typeface.BOLD)

            subparent2.addView(subparenttv2)

            //Isi UOM
            val subparent3 = LinearLayout(this@ActivityDetailOrder)
            subparent3.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.12f)
            subparent3.setOrientation(LinearLayout.VERTICAL)

            val subparenttv3 = TextView(this@ActivityDetailOrder)
            subparenttv3.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv3.setText(""+enttitas.mUom)
            subparenttv3.setTypeface(subparenttv3.getTypeface(), Typeface.BOLD)

            subparent3.addView(subparenttv3)

            //Isi PRICE
            val subparent4 = LinearLayout(this@ActivityDetailOrder)
            subparent4.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.25f)
            subparent4.setOrientation(LinearLayout.VERTICAL)

            val subparenttv4 = TextView(this@ActivityDetailOrder)
            subparenttv4.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv4.setText(""+enttitas.mPrice)

            subparent4.addView(subparenttv4)

            //Isi TOTAL
            val subparent5 = LinearLayout(this@ActivityDetailOrder)
            subparent5.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.26f)
            subparent5.setOrientation(LinearLayout.VERTICAL)

            val subparenttv5 = TextView(this@ActivityDetailOrder)
            subparenttv5.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv5.setText(""+enttitas.mTotal)

            subparent5.addView(subparenttv5)

            subparent.addView(subparent1)
            subparent.addView(subparent2)
            subparent.addView(subparent3)
            subparent.addView(subparent4)
            subparent.addView(subparent5)

            val v1 = View(this@ActivityDetailOrder)
            v1.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2)
            v1.setBackgroundColor(getResources().getColor(R.color.colorYellowTextButton))
            v1.setTag(enttitas)

            //Layout Vertical Detail
            val subparentDetail = LinearLayout(this@ActivityDetailOrder)
            subparentDetail.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            subparentDetail.setOrientation(LinearLayout.VERTICAL)
            subparentDetail.setTag(enttitas)

            val subparentstv1 = TextView(this@ActivityDetailOrder)
            subparentstv1.setTextColor(getResources().getColor(R.color.colorBlack))
            subparentstv1.setText(""+enttitas.mDetail)
            subparentstv1.setTypeface(subparentstv1.getTypeface(), Typeface.BOLD_ITALIC)

            subparentDetail.addView(subparentstv1)

            val v2 = View(this@ActivityDetailOrder)
            v2.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2)
            v2.setBackgroundColor(getResources().getColor(R.color.colorGrayBackground))
            v2.setTag(enttitas)

            detail_order_layout_item.addView(subparent)
            detail_order_layout_item.addView(v1)
            detail_order_layout_item.addView(subparentDetail)
            detail_order_layout_item.addView(v2)
        }

    }
}