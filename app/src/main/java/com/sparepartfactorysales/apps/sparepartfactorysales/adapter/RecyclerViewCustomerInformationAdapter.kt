package com.sparepartfactorysales.apps.sparepartfactorysales.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sparepartfactorysales.apps.sparepartfactorysales.R
import kotlinx.android.synthetic.main.customer_information_item_list.view.*

class RecyclerViewCustomerInformationAdapter(var getData: ArrayList<String>, val context: Context, val listener: RecyclerViewAdapterListener): RecyclerView.Adapter<RecyclerViewCustomerInformationAdapter.ViewHolder>() {

    val onClickListeners: RecyclerViewAdapterListener = listener
    interface RecyclerViewAdapterListener{
        fun getData()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        // membuat view baru
        val v: View = LayoutInflater.from(p0.getContext()).inflate(R.layout.customer_information_item_list, p0, false)
        // mengeset ukuran view, margin, padding, dan parameter layout lainnya
        val vh = ViewHolder(v)

        return vh
    }

    override fun getItemCount(): Int {
        return getData.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.itemView.customer_information_item_list_value.tag
        p0.itemView.customer_information_item_list_value.text = getData.get(p1)
        p0.itemView.customer_information_item_list_value.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                onClickListeners.getData()
            }

        })
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}