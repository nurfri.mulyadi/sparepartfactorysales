package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ExpandableListView
import android.widget.Toast
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.ExpandableListOrderCatalog
import kotlinx.android.synthetic.main.activity_done_order_catalog.*
import kotlinx.android.synthetic.main.toolbar_order.*
import kotlinx.android.synthetic.main.toolbar_order.view.*

class ActivityDoneOrderCatalog: AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {
        when(p0){
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_order ->{
                ActivityOrder.launchIntent(this, mflag)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
            done_order_button_confirm ->{
                ActivityOrder.launchIntent(this, mflag)
            }
        }
    }

    companion object {
        val REQUEST_CODE = 15
        var mImage: Int? = null
        var mflag: String? = null
        fun launchIntent(caller: Activity?, image: Int?, flag: String?){
            val intent = Intent(caller, ActivityDoneOrderCatalog::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)

            mImage = image
            mflag = flag
        }
    }

    val titleBarDorpdown = ArrayList<String>()
    val childDropdown = HashMap<String, List<String>>()
    val contentDropdDown1 = ArrayList<String>()
    val contentDropdDown2 = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_done_order_catalog)

        toolbar_catalog_done_order.title_toolbar.text = "CATALOG"

        done_order_image_category_item.setImageResource(mImage!!)
        setClickListener()

        dropdownMenu()
    }

    fun setClickListener(){
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_order.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
        done_order_button_confirm.setOnClickListener(this)
    }

    fun dropdownMenu(){
        titleBarDorpdown.add("Daihatsu")
        titleBarDorpdown.add("Ford")

        contentDropdDown1.add("DWR-S01")
        contentDropdDown1.add("DWR-S02")
        contentDropdDown1.add("DWR-S03")

        contentDropdDown2.add("DWR-SS1")
        contentDropdDown2.add("DWR-SS2")
        contentDropdDown2.add("DWR-SS3")

        childDropdown.put(titleBarDorpdown.get(0), contentDropdDown1)
        childDropdown.put(titleBarDorpdown.get(1), contentDropdDown2)

        val listAdapter = ExpandableListOrderCatalog(this, titleBarDorpdown, childDropdown, object: ExpandableListOrderCatalog.ExpandableListviewAdapterListener{
            override fun getDataKurang(data: String) {

            }

            override fun getDataTambah(data: String) {

            }

        })
        dropdown_order_catalog.setAdapter(listAdapter)

        dropdown_order_catalog!!.setOnGroupExpandListener { groupPosition ->

        }

        dropdown_order_catalog!!.setOnGroupCollapseListener { groupPosition ->

        }

        dropdown_order_catalog!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
            Toast.makeText(applicationContext, "Clicked: " + (titleBarDorpdown)[groupPosition] + " -> " + childDropdown[(titleBarDorpdown)[groupPosition]]!!.get(childPosition), Toast.LENGTH_SHORT).show()
            false
        }

    }
}