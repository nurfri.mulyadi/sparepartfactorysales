package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.RecyclerViewAllPromoAdapter
import com.sparepartfactorysales.apps.sparepartfactorysales.model.RecyclerViewModelAllPromo
import kotlinx.android.synthetic.main.activity_allpromo.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import kotlin.properties.Delegates

class ActivityAllPromo: AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {
        when(p0){
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    companion object {
        val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityAllPromo::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }
    }

    val dataAllPromo = ArrayList<RecyclerViewModelAllPromo>()
    var adapter by Delegates.notNull<RecyclerViewAllPromoAdapter>()
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_allpromo)

        toolbar_allpromo.title_toolbar.text = "ALL PROMO"

        dataAllPromo.add(RecyclerViewModelAllPromo("NEW ITEM-BRAKE LINING", "10-12-2018", "12:02:22"))
        dataAllPromo.add(RecyclerViewModelAllPromo("NEW ITEM-CONTAINER", "08-12-2018", "14:12:32"))
        dataAllPromo.add(RecyclerViewModelAllPromo("PROMO RACER NASIONAL 2018", "01-11-2018", "15:34:11"))


        adapter = RecyclerViewAllPromoAdapter(dataAllPromo, this@ActivityAllPromo, object: RecyclerViewAllPromoAdapter.RecyclerViewAdapterListener{
            override fun getData() {
                ActivityDescriptionProduct.launchIntent(this@ActivityAllPromo)
            }

        })
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this@ActivityAllPromo)
        allpromo_recyclerview.layoutManager = layoutManager
        allpromo_recyclerview.adapter = adapter

        setClickListener()
    }

    fun setClickListener(){
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }

}