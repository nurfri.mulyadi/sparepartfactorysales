package com.sparepartfactorysales.apps.sparepartfactorysales.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.LayoutInflater
import android.view.ViewGroup
import com.sparepartfactorysales.apps.sparepartfactorysales.R
import android.support.v4.content.ContextCompat.getSystemService
import kotlinx.android.synthetic.main.custom_layout_banner.view.*
import android.support.v4.view.ViewPager




class BannerPagerAdapter(context: Context): PagerAdapter() {

    private val context = context

    private var layoutInflater: LayoutInflater? = null

    private val images = arrayOf<Int>(R.drawable.button_birkens, R.drawable.button_birkens)


    override fun isViewFromObject(p0: View, p1: Any): Boolean {
        return p0 == p1
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layoutInflater!!.inflate(R.layout.custom_layout_banner, null)
        view.imageView.setImageResource(images[position])

        val vp = container as ViewPager
        vp.addView(view, 0)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }


}