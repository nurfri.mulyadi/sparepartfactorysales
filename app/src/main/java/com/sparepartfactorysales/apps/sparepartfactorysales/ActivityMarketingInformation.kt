package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.GridViewMarketingInformationAdapter
import com.sparepartfactorysales.apps.sparepartfactorysales.model.ItemCatalog
import kotlinx.android.synthetic.main.activity_marketing_information.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*

class ActivityMarketingInformation: AppCompatActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when(p0){
            marketing_information_button_news ->{
                ActivityProductKnwoledgeListItem.launchIntentNews(this@ActivityMarketingInformation, "news")
            }
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    companion object {
        /*val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityMarketingInformation::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }*/
    }

    val ImageBrands = ArrayList<ItemCatalog>()
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_marketing_information)
        toolbar_marketing_information.title_toolbar.text = "PRODUCT KNOWLEDGE"

        setClickListener()
        setGridView()
    }

    fun setClickListener(){
        marketing_information_button_news.setOnClickListener(this)
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }

    fun setGridView(){
        ImageBrands.add(ItemCatalog(R.drawable.button_birkens, "birkens"))
        ImageBrands.add(ItemCatalog(R.drawable.button_ikybi, "ikybi"))
        ImageBrands.add(ItemCatalog(R.drawable.button_heiker, "heiker"))
        ImageBrands.add(ItemCatalog(R.drawable.button_stecker, "stecker"))

        val adapter = GridViewMarketingInformationAdapter(this,ImageBrands, object: GridViewMarketingInformationAdapter.GridViewAdapterListener{
            override fun getData(image: Int) {
                ActivityProductKnwoledgeListItem.launchIntent(this@ActivityMarketingInformation, image, "brand")
            }
        })
        marketing_information_gridview_menu.adapter = adapter
        /*marketing_information_gridview_menu.onItemClickListener = object: AdapterView.OnItemClickListener{
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(p2 == 0){
                    Toast.makeText(this@ActivityMarketingInformation, "TESS", Toast.LENGTH_SHORT).show()
                }else if(p2 == 1){
                    Toast.makeText(this@ActivityMarketingInformation, "TESS DUA", Toast.LENGTH_SHORT).show()
                }else if(p2 == 2){
                    Toast.makeText(this@ActivityMarketingInformation, "TESS TIGA", Toast.LENGTH_SHORT).show()
                }else if(p2 == 3){
                    Toast.makeText(this@ActivityMarketingInformation, "TESS EMPAT", Toast.LENGTH_SHORT).show()
                }
            }*/

        }
    }
