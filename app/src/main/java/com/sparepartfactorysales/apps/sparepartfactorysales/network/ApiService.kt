package com.sparepartfactorysales.apps.sparepartfactorysales.network

import retrofit2.Call
import retrofit2.http.GET
import okhttp3.ResponseBody



interface ApiService {

    @GET("posts")
    fun getPosts(): Call<ResponseBody>

}