package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.GridViewCatalogOrderCategoryAdapter
import kotlinx.android.synthetic.main.activity_catalog_order_category.*
import kotlinx.android.synthetic.main.toolbar_order.*
import kotlinx.android.synthetic.main.toolbar_order.view.*

class ActivityCatalogOrderCategory: AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {
        when(p0){
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_order ->{
                ActivityOrder.launchIntent(this, "else")
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    companion object {
        val REQUEST_CODE = 15
        var mImage: Int? = null
        var mflag: String? = null
        fun launchIntent(caller: Activity?, image: Int, flag: String?){
            val intent = Intent(caller, ActivityCatalogOrderCategory::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)

            mImage = image
            mflag = flag
        }
    }

    val ImageBrands = arrayOf<Int>(
            R.drawable.img_category1,
            R.drawable.img_category2,
            R.drawable.img_category3,
            R.drawable.img_category4,
            R.drawable.img_category5,
            R.drawable.img_category6)
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catalog_order_category)

        toolbar_catalog_order_category.title_toolbar.text = "CATALOG"

        catalog_order_category_logo_brand.setImageResource(mImage!!)
        setClickListener()
        setGridView()
    }

    fun setClickListener(){
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
        toolbar_button_order.setOnClickListener(this)
    }

    fun setGridView(){
        val adapter = GridViewCatalogOrderCategoryAdapter(this,ImageBrands, object: GridViewCatalogOrderCategoryAdapter.GridViewAdapterListener{
            override fun getData(image: Int) {
                ActivityDoneOrderCatalog.launchIntent(this@ActivityCatalogOrderCategory, image, mflag)
            }
        })
        catalog_order_category_gridview_menu.adapter = adapter
        /*marketing_information_gridview_menu.onItemClickListener = object: AdapterView.OnItemClickListener{
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(p2 == 0){
                    Toast.makeText(this@ActivityMarketingInformation, "TESS", Toast.LENGTH_SHORT).show()
                }else if(p2 == 1){
                    Toast.makeText(this@ActivityMarketingInformation, "TESS DUA", Toast.LENGTH_SHORT).show()
                }else if(p2 == 2){
                    Toast.makeText(this@ActivityMarketingInformation, "TESS TIGA", Toast.LENGTH_SHORT).show()
                }else if(p2 == 3){
                    Toast.makeText(this@ActivityMarketingInformation, "TESS EMPAT", Toast.LENGTH_SHORT).show()
                }
            }*/

    }


}