package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.GridViewMarketingInformationAdapter
import com.sparepartfactorysales.apps.sparepartfactorysales.model.ItemCatalog
import kotlinx.android.synthetic.main.activity_catalog.*
import kotlinx.android.synthetic.main.activity_marketing_information.*
import kotlinx.android.synthetic.main.toolbar_order.*
import kotlinx.android.synthetic.main.toolbar_order.view.*

class ActivityCatalog: AppCompatActivity(), View.OnClickListener {
    companion object {
        val REQUEST_CODE = 15
        var mflag: String? = null
        fun launchIntent(caller: Activity?, flag: String?){
            val intent = Intent(caller, ActivityCatalog::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)

            mflag = flag
        }
    }

    override fun onClick(p0: View?) {
        when(p0){
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_order ->{
                ActivityOrder.launchIntent(this, "else")
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    val ImageBrands = ArrayList<ItemCatalog>()
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catalog)
        toolbar_catalog.title_toolbar.text = "CATALOG"

        setClickListener()
        setGridView()
    }

    fun setClickListener(){
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
        toolbar_button_order.setOnClickListener(this)
    }

    fun setGridView(){
        ImageBrands.add(ItemCatalog(R.drawable.button_birkens, "birkens"))
        ImageBrands.add(ItemCatalog(R.drawable.button_ikybi, "ikybi"))
        ImageBrands.add(ItemCatalog(R.drawable.button_heiker, "heiker"))
        ImageBrands.add(ItemCatalog(R.drawable.button_stecker, "stecker"))

        val adapter = GridViewMarketingInformationAdapter(this,ImageBrands, object: GridViewMarketingInformationAdapter.GridViewAdapterListener{
            override fun getData(image: Int) {
                ActivityCatalogOrderCategory.launchIntent(this@ActivityCatalog, image, mflag)
            }


        })
        catalog_gridview_menu.adapter = adapter
        /*catalog_gridview_menu.onItemClickListener = object: AdapterView.OnItemClickListener{
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(p2 == 0){
                    Toast.makeText(this@ActivityCatalog, "TESS", Toast.LENGTH_SHORT).show()
                }else if(p2 == 1){
                    Toast.makeText(this@ActivityCatalog, "TESS DUA", Toast.LENGTH_SHORT).show()
                }else if(p2 == 2){
                    Toast.makeText(this@ActivityCatalog, "TESS TIGA", Toast.LENGTH_SHORT).show()
                }else if(p2 == 3){
                    Toast.makeText(this@ActivityCatalog, "TESS EMPAT", Toast.LENGTH_SHORT).show()
                }
            }

        }*/
        catalog_gridview_menu.isTextFilterEnabled = true
        catalog_input_customer.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                println("Text ["+s+"] - Start ["+start+"] - Before ["+before+"] - Count ["+count+"]")
                if(count < before){
                    // We're deleting char so we need to reset the adapter data
                    adapter.resetData()
                }
                adapter.filter.filter(s.toString())
            }

        })
    }

}