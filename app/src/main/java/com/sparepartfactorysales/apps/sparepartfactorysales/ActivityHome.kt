package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.GridViewMenuAdapter
import kotlinx.android.synthetic.main.activity_home.*
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import android.R.attr.keySet
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.sparepartfactorysales.apps.sparepartfactorysales.adapter.BannerPagerAdapter
import java.util.*


class ActivityHome: AppCompatActivity(), View.OnClickListener {

    companion object {
        //val REQUEST_CODE = 15
        /*fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityHome::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }*/
    }

    override fun onClick(p0: View?) {
        when(p0){
            home_button_profile ->{
                //ActivityProfil.launchIntent(this)
                startActivity(Intent(this@ActivityHome, ActivityProfil::class.java))
            }

            home_button_statistic ->{
                //ActivityStatistic.launchIntent(this)
                startActivity(Intent(this@ActivityHome, ActivityStatistic::class.java))
            }

            home_button_cart ->{
                ActivityOrder.launchIntent(this, "else")
            }

            home_button_logout ->{
                onBackPressed()
            }

            layout_button_promo ->{
                //ActivityAllPromo.launchIntent(this)
                startActivity(Intent(this@ActivityHome, ActivityAllPromo::class.java))
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        home_gridview_menu.adapter = GridViewMenuAdapter(this)
        home_gridview_menu.onItemClickListener = object: AdapterView.OnItemClickListener{
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(p2 == 0){
                    //ActivitySalesRoute.launchIntent(this@ActivityHome)
                    startActivity(Intent(this@ActivityHome, ActivitySalesRoute::class.java))
                }else if(p2 == 1){
                    ActivityCatalog.launchIntent(this@ActivityHome, "else")
                }else if(p2 == 2){
                    //ActivityCustomerInformation.launchIntent(this@ActivityHome)
                    startActivity(Intent(this@ActivityHome, ActivityCustomerInformation::class.java))
                }else if(p2 == 3){
                    //ActivityMarketingInformation.launchIntent(this@ActivityHome)
                    startActivity(Intent(this@ActivityHome, ActivityMarketingInformation::class.java))
                }
            }
        }

        sliderPromo()
        setClickListener()
    }

    fun setClickListener(){
        home_button_profile.setOnClickListener(this)
        home_button_statistic.setOnClickListener(this)
        home_button_cart.setOnClickListener(this)
        home_button_logout.setOnClickListener(this)
        layout_button_promo.setOnClickListener(this)
    }

    //slider promo
    fun sliderPromo(){
        // Load Image Dari res/drawable
        val file_maps = HashMap<String, Int>()
        file_maps["Slider Street 1"] = R.drawable.banner_promo
        file_maps["Slider Sunny 2"] = R.drawable.banner_promo

        for (name in file_maps.keys) {
            val textSliderView = TextSliderView(this)
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name)!!).scaleType = BaseSliderView.ScaleType.Fit
            //add your extra information
            textSliderView.bundle(Bundle())
            textSliderView.bundle
                    .putString("extra", name)
            slider.addSlider(textSliderView)
        }

        slider.setPresetTransformer(SliderLayout.Transformer.Accordion)
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
        slider.setCustomAnimation(DescriptionAnimation())
        slider.setDuration(4000)
    }
}