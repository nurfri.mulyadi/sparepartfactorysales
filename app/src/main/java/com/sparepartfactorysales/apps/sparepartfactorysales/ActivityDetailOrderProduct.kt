package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.sparepartfactorysales.apps.sparepartfactorysales.model.DetailOrderProductModel
import com.sparepartfactorysales.apps.sparepartfactorysales.model.OrderHistoryModel
import kotlinx.android.synthetic.main.activity_detail_order_product.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.util.ArrayList

class ActivityDetailOrderProduct: AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {
        when(p0){
            toolbar_button_home ->{
                val intent = Intent(applicationContext, ActivityHome::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            toolbar_button_back ->{
                onBackPressed()
            }
        }
    }

    companion   object {
        val REQUEST_CODE = 15
        var mSoNo: String? = null
        var mSoDate: String? = null
        var mStatus: String? = null
        fun launchIntent(caller: Activity?, soNo: String?, soDate: String?, status: String?){
            val intent = Intent(caller, ActivityDetailOrderProduct::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)

            mSoNo = soNo
            mSoDate = soDate
            mStatus = status
        }
    }

    val dataDetailOrderProduct = ArrayList<DetailOrderProductModel>()
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_order_product)

        toolbar_order_detail_product.title_toolbar.text = "ORDER DETAIL"

        setClickListener()
        createListStatusDetailProduct()
    }

    fun setClickListener(){
        toolbar_button_home.setOnClickListener(this)
        toolbar_button_back.setOnClickListener(this)
    }

    fun createListStatusDetailProduct(){

        detail_order_product_txt_soNo.text = ""+mSoNo
        detail_order_product_txt_soDate.text = ""+mSoDate
        detail_order_product_txt_status.text = ""+ mStatus


        dataDetailOrderProduct.add(DetailOrderProductModel("DWF-011", "DELTA(v22)FR RH(1 1/4)", "10", "Pcs"))
        dataDetailOrderProduct.add(DetailOrderProductModel("HYWR-002", "ATOZ RR LH(7/10)", "5", "Pcs"))
        dataDetailOrderProduct.add(DetailOrderProductModel("FDB-003", "RANGER 4x4 2500cc", "5", "Pcs"))

        for(enttitas in dataDetailOrderProduct) {
            //layout list horizontal
            val subparent = LinearLayout(this@ActivityDetailOrderProduct)
            subparent.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            subparent.setOrientation(LinearLayout.HORIZONTAL)
            subparent.setWeightSum(1f)
            subparent.setPadding(0, 40, 0, 40)
            subparent.setTag(enttitas)

            //Isi Code
            val subparent1 = LinearLayout(this@ActivityDetailOrderProduct)
            subparent1.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.25f)
            subparent1.setOrientation(LinearLayout.VERTICAL)

            val subparenttv1 = TextView(this@ActivityDetailOrderProduct)
            subparenttv1.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv1.setText("" + enttitas.mCode)
            subparenttv1.setTypeface(subparenttv1.getTypeface(), Typeface.BOLD)

            subparent1.addView(subparenttv1)

            //Isi Product
            val subparent2 = LinearLayout(this@ActivityDetailOrderProduct)
            subparent2.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.46f)
            subparent2.setOrientation(LinearLayout.VERTICAL)

            val subparenttv2 = TextView(this@ActivityDetailOrderProduct)
            subparenttv2.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv2.setText("" + enttitas.mProduct)
            subparenttv2.setTypeface(subparenttv2.getTypeface(), Typeface.BOLD)

            subparent2.addView(subparenttv2)

            //Isi Qty
            val subparent3 = LinearLayout(this@ActivityDetailOrderProduct)
            subparent3.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.12f)
            subparent3.setOrientation(LinearLayout.VERTICAL)

            val subparenttv3 = TextView(this@ActivityDetailOrderProduct)
            subparenttv3.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv3.setText("" + enttitas.mQty)
            subparenttv3.setTypeface(subparenttv3.getTypeface(), Typeface.BOLD)

            subparent3.addView(subparenttv3)

            //Isi Uom
            val subparent4 = LinearLayout(this@ActivityDetailOrderProduct)
            subparent4.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.12f)
            subparent4.setOrientation(LinearLayout.VERTICAL)

            val subparenttv4 = TextView(this@ActivityDetailOrderProduct)
            subparenttv4.setTextColor(getResources().getColor(R.color.colorBlack))
            subparenttv4.setText("" + enttitas.mUom)
            subparenttv4.setTypeface(subparenttv4.getTypeface(), Typeface.BOLD)

            subparent4.addView(subparenttv4)

            subparent.addView(subparent1)
            subparent.addView(subparent2)
            subparent.addView(subparent3)
            subparent.addView(subparent4)

            val v1 = View(this@ActivityDetailOrderProduct)
            v1.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2)
            v1.setBackgroundColor(getResources().getColor(R.color.colorGray))
            v1.setTag(enttitas)

            order_detail_product_layout_item2.addView(subparent)
            order_detail_product_layout_item2.addView(v1)
        }

    }
}