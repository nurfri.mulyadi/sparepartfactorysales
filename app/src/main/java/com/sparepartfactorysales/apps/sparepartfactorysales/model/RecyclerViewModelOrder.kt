package com.sparepartfactorysales.apps.sparepartfactorysales.model

class RecyclerViewModelOrder(image: Int?, customer: String?, category: String?, carMerk: String?, oemNo: String?, type: String?, quantity: String?, totalPrice: String?) {

    val mImage = image
    val mCustomer = customer
    val mCategory = category
    val mCarMerks = carMerk
    val mOemNo = oemNo
    val mtype = type
    val mquantity = quantity
    val mTotalPrice = totalPrice

}