package com.sparepartfactorysales.apps.sparepartfactorysales.model

class DetailOrderProductModel(code: String?, product: String?, qty: String?, uom: String?){

    val mCode = code
    val mProduct = product
    val mQty = qty
    val mUom = uom
}