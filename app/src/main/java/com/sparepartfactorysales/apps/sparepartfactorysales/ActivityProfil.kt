package com.sparepartfactorysales.apps.sparepartfactorysales

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_profil.*

class ActivityProfil: AppCompatActivity(), View.OnClickListener {

    companion object{
        val REQUEST_CODE = 15
        fun launchIntent(caller: Activity?){
            val intent = Intent(caller, ActivityProfil::class.java)
            caller!!.startActivityForResult(intent, REQUEST_CODE)
        }
    }

    override fun onClick(p0: View?) {
        when(p0){
            profil_button_back ->{
                onBackPressed()
            }

            profil_button_logout ->{
                val intent = Intent(applicationContext, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            profil_button_change_password->{
                startActivity(Intent(this@ActivityProfil, ActivityChangePassword::class.java))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profil)

        setClickListener()
    }

    fun setClickListener(){
        profil_button_back.setOnClickListener(this)
        profil_button_logout.setOnClickListener(this)
        profil_button_change_password.setOnClickListener(this)
    }

}